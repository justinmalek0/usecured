package uSecured;

public class Scan_Printf extends Source_Scan {

	public Scan_Printf(Controller c, Log l, Source_Input si) {
		super(c, l, si);
	}

	@Override
	public Scan_Printf newCopy(Source_Input si) {
		return new Scan_Printf(getController(), getLog(), si);
	}

	@Override
	public String toString() {
		return super.toString() + " Printf";
	}
	
	@Override
	public String SearchString(){
		return "printf";
	}

	@Override
	protected void check(Source_File file, String function, String[] args) {
		int idxFormatArg;
		switch( function ) {
		case "printf" : // printf ( const char * format, ... );
		case "vprintf" : // vprintf ( const char * format, va_list arg );
			idxFormatArg = 0;
			break;
		case "sprintf" : // sprintf ( char * str, const char * format, ... );
		case "vsprintf" : // vsprintf (char * s, const char * format, va_list arg );
			Add_To_List(file.Filename()+": Consider using the sized buffer version (" +
						function.substring(0, function.indexOf(SearchString()))+"n"+SearchString()+
						") instead of "+function+" to avoid potential buffer overflows to "+args[0]+".");
			// Intentional Case fall-through
		case "fprintf" : // fprintf ( FILE * stream, const char * format, ... );
		case "vfprintf" : // vfprintf ( FILE * stream, const char * format, va_list arg );
			idxFormatArg = 1;
			break;
		case "snprintf" : // snprintf ( char * s, size_t n, const char * format, ... );
		case "vsnprintf" : // vsnprintf (char * s, size_t n, const char * format, va_list arg );
			idxFormatArg = 2;
			break;
		default :
			return;
		}
		
		if( idxFormatArg >= args.length ){
			return;
		}
		
		if ( !args[idxFormatArg].contains("\"") ){
			Add_To_List(file.Filename()+": "+function+" with format "+args[idxFormatArg]+" is potentially unsafe!");
		}
	}

}
