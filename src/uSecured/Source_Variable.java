package uSecured;

public class Source_Variable {
	
	private Variable_Type type;
	private String named_type;
	private String name;
	private Scope scope;

	public Source_Variable( Scope s, String nt, String n ) {
		Type(Variable_Type.NOT_SET);
		Name(n);
		Scope(s);
		Named_Type(nt);
	}

	public Variable_Type Type() {
		return type;
	}

	public Source_Variable Type(Variable_Type type) {
		this.type = type;
		return this;
	}

	public String Name() {
		if(name == null){
			return "";
		}
		return name;
	}

	public Source_Variable Name(String name) {
		this.name = name;
		return this;
	}

	public Scope Scope() {
		return scope;
	}

	public Source_Variable Scope(Scope scope) {
		this.scope = scope;
		return this;
	}

	public String Named_Type() {
		if(named_type == null){
			return "";
		}
		return named_type;
	}

	public Source_Variable Named_Type(String named_type) {
		this.named_type = named_type;
		return this;
	}

	@Override
	public String toString() {
		return "Variable: " + Named_Type() + " " + Name() + ";";
	}
	
	public static Source_Variable varFromString( Scope scp, String s ) {
		
		String[] splits = s.split("( |\r|\n|*|const |static |extern |;)+");
		
		if(splits.length < 2){
			return null;
		}
		
		return new Source_Variable(scp, splits[0], splits[1]);
	}

}
