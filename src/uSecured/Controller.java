package uSecured;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

public class Controller extends MicroSecured_Thread {

    // DESIGN: Bin_Input and Source_Input should be in the same class heirarchy 
    // but I may not do it now to deal with the time restriction that is looming
	
	public static final String THREAD_NAME = "uSecured Controller";

    private MicroSecured mainGUI;
    private List<Analysis_Test> List_Tests;
    private List<Analysis_Test> List_Tests_Completed;
    private List<Bin_Input> List_Files;
    private List<Bin_Input> List_Files_Completed;
    private List<Source_Input> List_Src_Files;
    private List<Source_Input> List_Src_Files_Completed;
    private int progress_inc;
    private int progress;
    
    private int count_obj_tests = 0;
    private int count_runtime_tests = 0;
    private boolean runtime_test_running = false;

	public Controller(int q_size,Log l, MicroSecured ms) {
		setMsgQ( new ArrayBlockingQueue<Message>( q_size ) );
		setLog(l);
		set_List_Tests( new ArrayList<Analysis_Test>( ) );
		setList_Tests_Completed(new ArrayList<Analysis_Test>( ));
		set_List_Files( new ArrayList<Bin_Input>( ) );
		setList_Files_Completed( new ArrayList<Bin_Input>( ) );
		setList_Src_Files(new ArrayList<Source_Input>());
		setList_Src_Files_Completed(new ArrayList<Source_Input>());
        mainGUI = ms;
        progress_inc = 100;
        progress = -1;
        
        setController(this);
        
        start();
	}
    
    @Override
    public void run() {
        while ( isRunning() )
        {
        	try {
				Message msg = recvMessage();
				
				// msgLog( new Message( "CTLR MSG: " + msg.getMsg() ) );
				
				switch( msg.getType() )
				{
                    case Test :
						Tests_Message omsg = (Tests_Message)msg;
                        boolean b_Add_New_Test = true;
                    	for ( Analysis_Test at : List_Tests ) {
                            if ( omsg.getTest().equals(at) )
                            {
                            	msgLog( omsg.getTest() + " was already added." );
                            	b_Add_New_Test = false;
                            }
                        }
                    	if(b_Add_New_Test)
                    	{
                        	// msgLog( new Message( omsg.get_Test() + " was added to the test list.") );
                    		List_Tests.add(omsg.getTest());
                    		if ( ( omsg.getTest() instanceof Runtime_Web ) && 
                    			 ( ((Runtime_Web)omsg.getTest()).getInputTest() != null ) ) {
                    			List_Tests.add(((Runtime_Web)omsg.getTest()).getInputTest());
                    		}
                    		mainGUI.update_test_list(omsg.getTest());
                    		msgLog( List_Tests.get(List_Tests.size()-1) + " added to the test list." );
                    	}
						break;
					case Obj_File :
						boolean dup_file = false;
						File_Message fmsg = (File_Message)msg;
						for( Bin_Input in : get_List_Files() )
						{
							if ( in.get_File().getAbsolutePath().equals(fmsg.get_File().getAbsolutePath()) )
							{
								msgLog(  "Bin File (" + fmsg.get_File().getAbsolutePath() + ") is a duplicate and was not added" );
								dup_file = true;
							}
						}
						if ( !dup_file )
						{
							// create Zip/Bin input types here correctly
							Bin_Input bi = fmsg.getInput();
							if (!( fmsg.get_Decoder() instanceof No_Decoder )) {
								bi.set_Decoder(fmsg.get_Decoder());
							}
							this.get_List_Files().add( bi  );
							msgLog( "Bin File (" + fmsg.get_File().getAbsolutePath() + ") added." );
						}
						break;
                    case Source_File :
						boolean dup_src = false;
                        Source_File_Message sfm = (Source_File_Message)msg;
                        for( Source_Input sin : getList_Src_Files()) {
                        	if( sin.equals(sfm.getSourceInput())) {
								msgLog(  "Source File (" + sin.getFile().getAbsolutePath() + ") is a duplicate and was not added" );
                        		dup_src = true;
                        	}
                        }
                        if(!dup_src) {
                        	Source_Input sin = sfm.getSourceInput();
                        	getList_Src_Files().add(sin);
                        	msgLog("Source File ("+sin.getFile().getAbsolutePath()+") added.");
                        }
                        break;
                    case Start_Tests :
                    	// Start Bin_Input threads if necessary
                    	// Start Parser threads if necessary (linking Bin_Input to Parser)
                    	// Object Code Tests
                        int thread_count = 0;
                        count_obj_tests = 0;
                        count_runtime_tests = 0;
                    	for( Bin_Input bin : List_Files ) {
                            bin.create();
                            thread_count++;
                            // Start Compression/Encryption if necessary
                            if ( bin.get_Decoder() != null ) {
                                thread_count++;
                            }
                            Parser p = new Parser( 500, this, getLog() );
                            thread_count++;
                            bin.setMsgQ(p.getMsgQ());

                            // Start Tests
                            for( Analysis_Test at : List_Tests ) {
                                // Link the Parser to the object code test
                                if ( at instanceof Obj_Code ) {
                                    Obj_Code oc = (Obj_Code)at;
                                    if ( oc.get_File().equals(bin.get_File()) ) {
                                    	// Link Obj File Tests Input queues to necessary Parser threads
                    					p.add_Test(at);
                                        oc.start();
                                        thread_count++;
                                        count_obj_tests++;
                    				}
                    			}
                    		}
                    	}
                    	
                    	runtime_test_running = false; 
                    	// Start Runtime Tests
                        for( Analysis_Test at : List_Tests ) {
                            // Link the Parser to the object code test
                            if ( at instanceof Runtime_Web ) {
                            	if ( count_obj_tests == 0 && !runtime_test_running ) {
                            		at.start();
                            		runtime_test_running = true;
                            	}
                            	thread_count++;
                            	count_runtime_tests++;
                            }
                        }
                        
                        // Start Source_Input, Parser, and Source_Scan Tests...
                        for(Source_Input sin : List_Src_Files){
                        	sin.create();
                        	thread_count++;
                        	Parser p = new Parser(500, this, getLog());
                        	thread_count++;
                        	sin.setMsgQ(p.getMsgQ());
                        	for(Analysis_Test at : List_Tests){
                        		if ( at instanceof Source_Scan) {
                        			Source_Scan ss = (Source_Scan)at;
                        			if( ss.getInput().equals(sin)) {
                        				p.add_Test(at);
                        				ss.start();
                        				thread_count++;
                        			}
                        		}
                        	}
                        }
                    	
                    	progress_inc = (int)( 100 / thread_count );
                    	//msgLog(new Message("CTLR: progress_inc = "+progress_inc+" with thread count = "+thread_count));
                    	
                    	for( Bin_Input in : List_Files ) {
                    		in.start_run();
                    	}
                    	for( Source_Input sin : List_Src_Files ) {
                    		sin.start_run();
                    	}
                        break;
                    case Thread_Complete :
						Thread_Complete_Message tmsg = (Thread_Complete_Message)msg;
						//msgLog(new Message("CTLR: got Thread_Complete_Message"));

        				switch(tmsg.get_MS_Thread().get_Thread_Name()){
        					case Bin_Input.THREAD_NAME:
        					case Zip_Input.THREAD_NAME:
        					case GZip_Input.THREAD_NAME:
                                Iterator<Bin_Input> iter_bin = List_Files.iterator();

                                while(iter_bin.hasNext()) {
                                	Bin_Input in = iter_bin.next();
                            		if(!in.isRunning()) {
                                        in.getThread().join();
                            			//msgLog(new Message("CTLR: removing "+tmsg.get_MS_Thread().get_Thread_Name()));
                            			List_Files_Completed.add(in);
                            			iter_bin.remove();
                                        update_progress();
                            		}
                            	}
            					break;
        					case Source_Input.THREAD_NAME:
                                Iterator<Source_Input> iter_src = List_Src_Files.iterator();

                                while(iter_src.hasNext()) {
                                	Source_Input in = iter_src.next();
                            		if(!in.isRunning()) {
                                        in.getThread().join();
                            			//msgLog(new Message("CTLR: removing "+tmsg.get_MS_Thread().get_Thread_Name()));
                            			List_Src_Files_Completed.add(in);
                            			iter_src.remove();
                                        update_progress();
                            		}
                            	}
        						break;
                            case Parser.THREAD_NAME:
                                //msgLog(new Message("CTLR: removing Parser"));
                                update_progress();
                                break;
            				case Decoder.THREAD_NAME:
                    			//msgLog(new Message("CTLR: removing Decoder"));
                                update_progress();
            					break;
            				case Analysis_Test.THREAD_NAME:
                                Iterator<Analysis_Test> iter_test = List_Tests.iterator();

                                while(iter_test.hasNext()) {
                                    Analysis_Test at = iter_test.next();
                        			if(at.equals(tmsg.get_MS_Thread())) {
                                        at.getThread().join();
                        				msgLog(at+" Results:"+System.lineSeparator()+at.Hits_To_String());
                            			//msgLog(new Message("CTLR: removing "+at));
                            			if( at instanceof Obj_Code ) {
                            				count_obj_tests--;
                            			} else if ( at instanceof Runtime_Web ) {
                            				count_runtime_tests--;
                            				runtime_test_running = false;
                            			}
                            			List_Tests_Completed.add(at);
                            			iter_test.remove();
                                        update_progress();
                        			}
                        		}
                                
                                runtime_test_running = startNextRuntimeTest();
                        		break;
                            default :
                                break;
        				}


                		if ( List_Files.isEmpty() && List_Tests.isEmpty() && List_Src_Files.isEmpty() ) {
                			finish();
                		}
                        break;
                    default :
                        msgLog("CTLR: Unhandled Message Type!");
                        break;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
    }

	private boolean startNextRuntimeTest() {
        if ( count_obj_tests == 0 && count_runtime_tests > 0 && !runtime_test_running ) {
	        for( Analysis_Test at : List_Tests ) {
	            if ( at instanceof Runtime_Web ) {
	            	at.start();
	            	return true;
	            }
	        }
        }
        return false;
	}

	@Override
	public void finish() {
        mainGUI.update_progress(100);
        progress = -1;
        progress_inc = 100;
    }

	public List<Analysis_Test> getList_Tests_Completed() {
		return List_Tests_Completed;
	}

	public void setList_Tests_Completed(List<Analysis_Test> list_Tests_Completed) {
		List_Tests_Completed = list_Tests_Completed;
	}

	public List<Analysis_Test> get_List_Tests() {
        return List_Tests;
    }

    private void set_List_Tests(List<Analysis_Test> list_Tests) {
        List_Tests = list_Tests;
    }

	public List<Bin_Input> getList_Files_Completed() {
		return List_Files_Completed;
	}

	public void setList_Files_Completed(List<Bin_Input> list_Files_Completed) {
		List_Files_Completed = list_Files_Completed;
	}

	public List<Bin_Input> get_List_Files() {
		return List_Files;
	}

	public void set_List_Files(List<Bin_Input> list_Files) {
		List_Files = list_Files;
	}

    public List<Source_Input> getList_Src_Files() {
		return List_Src_Files;
	}

	public void setList_Src_Files(List<Source_Input> list_Src_Files) {
		List_Src_Files = list_Src_Files;
	}

	public List<Source_Input> getList_Src_Files_Completed() {
		return List_Src_Files_Completed;
	}

	public void setList_Src_Files_Completed(
			List<Source_Input> list_Src_Files_Completed) {
		List_Src_Files_Completed = list_Src_Files_Completed;
	}

	public void message( Message m )
    {
        try {
			getMsgQ().put( m );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

	@Override
	public String toString() {
        String result = "";
        
        for ( Analysis_Test at : List_Tests )
        {
        	//msgLog(new Message( at.toString() ));
            result.concat( at + "\r\n" );
        }
        
        return result;
	}
	
	private void update_progress() {
		progress += progress_inc;
		mainGUI.update_progress(progress);
	}

	@Override
	public String get_Thread_Name() {
		return Controller.THREAD_NAME;
	}

	@Override
	public Controller newCopy() {
		return new Controller(getMsgQ().size(), getLog(), mainGUI);
	}
	
}
