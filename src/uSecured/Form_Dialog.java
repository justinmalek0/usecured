package uSecured;

import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Enumeration;

import javax.swing.JScrollPane;
import javax.swing.JList;

public class Form_Dialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6401019356543674858L;
	
	private Form form = null;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtAction;
	private JLabel lblNameoptional;
	private JTextField txtName;
	private JLabel lblMethod;
	private JComboBox<String> comboMethod;
	private JLabel lblAction;
	private final JScrollPane scrollItem = new JScrollPane();
	private JList<FormItem> listItems;

	private DefaultListModel<FormItem> listModelItems;
	private JLabel lblCredential;
	private JLabel lblParentURL;
	private JTextField txtParentURL;
	private JComboBox<Credential> comboCredential;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// 	try {
	// 		Form_Dialog dialog = new Form_Dialog();
	// 		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	// 		dialog.setVisible(true);
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 	}
	// }

	/**
	 * Create the dialog.
	 */
	public Form_Dialog(Window own, DefaultListModel<Credential> list_cred) {
		super(own, "New Web Form", Dialog.ModalityType.APPLICATION_MODAL);
		setResizable(false);
		setBounds(100, 100, 302, 364);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 296, 147);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] {120, 50};
		gbl_contentPanel.rowHeights = new int[] {20, 20, 20, 20, 20};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		contentPanel.setLayout(gbl_contentPanel);
		{
			lblNameoptional = new JLabel("Name (optional)");
			GridBagConstraints gbc_lblNameoptional = new GridBagConstraints();
			gbc_lblNameoptional.anchor = GridBagConstraints.EAST;
			gbc_lblNameoptional.insets = new Insets(0, 0, 5, 5);
			gbc_lblNameoptional.gridx = 0;
			gbc_lblNameoptional.gridy = 0;
			contentPanel.add(lblNameoptional, gbc_lblNameoptional);
		}
		{
			txtName = new JTextField();
			GridBagConstraints gbc_txtName = new GridBagConstraints();
			gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtName.insets = new Insets(0, 0, 5, 0);
			gbc_txtName.gridx = 1;
			gbc_txtName.gridy = 0;
			contentPanel.add(txtName, gbc_txtName);
			txtName.setColumns(10);
		}
		{
			lblMethod = new JLabel("Method");
			GridBagConstraints gbc_lblMethod = new GridBagConstraints();
			gbc_lblMethod.anchor = GridBagConstraints.EAST;
			gbc_lblMethod.insets = new Insets(0, 0, 5, 5);
			gbc_lblMethod.gridx = 0;
			gbc_lblMethod.gridy = 1;
			contentPanel.add(lblMethod, gbc_lblMethod);
		}
		{
			comboMethod = new JComboBox<String>();
			comboMethod.setMaximumRowCount(2);
			comboMethod.setModel(new DefaultComboBoxModel<String>(new String[] {"GET", "POST"}));
			comboMethod.setSelectedIndex(0);
			GridBagConstraints gbc_comboMethod = new GridBagConstraints();
			gbc_comboMethod.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboMethod.insets = new Insets(0, 0, 5, 0);
			gbc_comboMethod.gridx = 1;
			gbc_comboMethod.gridy = 1;
			contentPanel.add(comboMethod, gbc_comboMethod);
		}
		{
			lblAction = new JLabel("Action");
			GridBagConstraints gbc_lblAction = new GridBagConstraints();
			gbc_lblAction.anchor = GridBagConstraints.EAST;
			gbc_lblAction.insets = new Insets(0, 0, 5, 5);
			gbc_lblAction.gridx = 0;
			gbc_lblAction.gridy = 2;
			contentPanel.add(lblAction, gbc_lblAction);
		}
		{
			txtAction = new JTextField();
			GridBagConstraints gbc_txtAction = new GridBagConstraints();
			gbc_txtAction.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtAction.insets = new Insets(0, 0, 5, 0);
			gbc_txtAction.gridx = 1;
			gbc_txtAction.gridy = 2;
			contentPanel.add(txtAction, gbc_txtAction);
			txtAction.setColumns(10);
		}
		
		lblParentURL = new JLabel("Parent URL");
		GridBagConstraints gbc_lblParentURL = new GridBagConstraints();
		gbc_lblParentURL.anchor = GridBagConstraints.EAST;
		gbc_lblParentURL.insets = new Insets(0, 0, 5, 5);
		gbc_lblParentURL.gridx = 0;
		gbc_lblParentURL.gridy = 3;
		contentPanel.add(lblParentURL, gbc_lblParentURL);
		{
			txtParentURL = new JTextField();
			GridBagConstraints gbc_txtParentURL = new GridBagConstraints();
			gbc_txtParentURL.insets = new Insets(0, 0, 5, 0);
			gbc_txtParentURL.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtParentURL.gridx = 1;
			gbc_txtParentURL.gridy = 3;
			contentPanel.add(txtParentURL, gbc_txtParentURL);
			txtParentURL.setColumns(10);
		}
		{
			lblCredential = new JLabel("Credential");
			GridBagConstraints gbc_lblCredential = new GridBagConstraints();
			gbc_lblCredential.anchor = GridBagConstraints.EAST;
			gbc_lblCredential.insets = new Insets(0, 0, 0, 5);
			gbc_lblCredential.gridx = 0;
			gbc_lblCredential.gridy = 4;
			contentPanel.add(lblCredential, gbc_lblCredential);
		}
		
		comboCredential = new JComboBox<Credential>();
		for( int i = 0; i < list_cred.size() ; i++ ) {
			comboCredential.addItem(list_cred.get(i));
		}
		GridBagConstraints gbc_comboCredential = new GridBagConstraints();
		gbc_comboCredential.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboCredential.gridx = 1;
		gbc_comboCredential.gridy = 4;
		contentPanel.add(comboCredential, gbc_comboCredential);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 303, 296, 33);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			{
				JButton okButton = new JButton("Add Form");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						form = null;
						if( txtName.getText().length() > 0 && txtAction.getText().length() > 0 && !listModelItems.isEmpty()) {
							if ( ((String)comboMethod.getSelectedItem()).length() > 0 ) {
								form = new Form( ((String)comboMethod.getSelectedItem()).toLowerCase(), txtName.getText(), txtAction.getText() );
							} else {
								form = new Form( null, txtName.getText(), txtAction.getText() );
							}

							if(!txtParentURL.getText().isEmpty()){
								form.FromURL(txtParentURL.getText());
							}
							if(comboCredential.getSelectedIndex() > -1){
								form.Credential((Credential)comboCredential.getSelectedItem());
							}
							
							// Arrays.asList(listModelItems.toArray());
							Enumeration<FormItem> items = listModelItems.elements();
							while ( items.hasMoreElements() ) {
								form.Add_Form_Item(items.nextElement());
							}
						}
						
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						form = null;
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
		scrollItem.setBounds(10, 150, 276, 107);
		getContentPane().add(scrollItem);

        listModelItems = new DefaultListModel<FormItem>();
		listItems = new JList<FormItem>(listModelItems);
		scrollItem.setViewportView(listItems);
		
		JButton btnAddFormItem = new JButton("Add Form Item");
		btnAddFormItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
        		FormItem_Dialog fd = new FormItem_Dialog(getOwner());

            	fd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            	fd.setVisible(true);
        		
            	if(fd.getItem() != null) {
            		listModelItems.addElement(fd.getItem());
            	}
			}
		});
		btnAddFormItem.setBounds(10, 269, 276, 23);
		getContentPane().add(btnAddFormItem);
	}
	
	public Form getForm() {
		return form;
	}
}
