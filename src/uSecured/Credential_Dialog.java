package uSecured;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Credential_Dialog extends JDialog {

	private static final long serialVersionUID = -4684624985510476924L;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtUser;
	private JTextField txtPwd;
	
	private Credential cred = null;

//	public static void main(String[] args) {
//		try {
//			Credential_Dialog dialog = new Credential_Dialog();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public Credential_Dialog(Window own) {
		super(own, "New Credential", Dialog.ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 237, 134);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		{
			JLabel lblUsername = new JLabel("Username");
			contentPanel.add(lblUsername);
		}
		{
			txtUser = new JTextField();
			contentPanel.add(txtUser);
			txtUser.setColumns(12);
		}
		{
			JLabel lblPassword = new JLabel("Password");
			contentPanel.add(lblPassword);
		}
		{
			txtPwd = new JTextField();
			contentPanel.add(txtPwd);
			txtPwd.setColumns(12);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Add Crendential");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(txtUser.getText().length() > 0 && txtPwd.getText().length() > 0) {
							cred = new Credential(txtUser.getText(), txtPwd.getText());
							dispose();
						}
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cred = null;
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

	public Credential getCredential() {
		return cred;
	}

}
