package uSecured;

public class Scan_Strcat extends Source_Scan {

	public Scan_Strcat(Controller c, Log l, Source_Input si) {
		super(c, l, si);
	}

	@Override
	public String SearchString() {
		return "strcat";
	}

	@Override
	protected void check(Source_File file, String function, String[] args) {
		if(function.equals(SearchString())){
			Add_To_List(file.Filename()+": Consider using strncat in place of "+SearchString()+" or "+args[0]+" could overflow.");
		}
	}

	@Override
	public Scan_Strcat newCopy(Source_Input si) {
		return new Scan_Strcat(getController(), getLog(), si);
	}

	@Override
	public String toString() {
		return super.toString() + " String Concatenate";
	}

}
