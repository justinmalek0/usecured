package uSecured;

public class Message {
	private final Message_Type type;
	private final String msg;
	
	public Message ( Message_Type t, String m )
	{
		type = t;
		msg = m;
	}
	
	public Message ( String m )
	{
		type = Message_Type.String_Message;
		msg = m;
	}

	public String getMsg() {
		return msg;
	}

	public Message_Type getType() {
		return type;
	}
	
	public String toString()
	{
		return getMsg();
	}
}
