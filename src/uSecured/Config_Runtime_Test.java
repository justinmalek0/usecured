package uSecured;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class Config_Runtime_Test extends JFrame {

	private static final long serialVersionUID = -1056895872339967657L;

	private Controller ctlr;
	private Log logging;
	
	private JPanel contentPane;
    
    /* Labels! */
    private JLabel lblTestList;
    private JLabel lblIPAddress;
    private JLabel lblCredential;
    private JLabel lblWebpages;
    private JLabel lblForms;
    
    /* Buttons! */
    private JButton btnSave;
    private JButton btnExit;
    private JButton btnAddCred;

    /* Text Fields */
    private JTextField txtUrl;
    
    /* Lists! */
    private JList<Runtime_Web> listRunTests;
    private final JScrollPane scrollTests;
    private final JScrollPane scrollCred;
    private DefaultListModel<Credential> listCred;
    private JList<Credential> listCredentials;

    /* Checkboxes*/
    private JCheckBox chckbxUseObjectCode;

    /* Text area with Scrollbars */
    private JTextArea txtrPages;
    private final JScrollPane scrollPages;
    
    private List<Runtime_Web> List_Active_Tests;
    private Bin_Input inputHitsFrom;
    private JButton btnForm;
    private JScrollPane scrollForm;
    private DefaultListModel<Form> listFrm= new DefaultListModel<Form>();
    private JList<Form> listForms;
    
	/**
	 * Create the frame.
	 */
	public Config_Runtime_Test( Controller c,Log l ) {
        ctlr = c;
        logging = l;

        List_Active_Tests = new ArrayList<Runtime_Web>();
        inputHitsFrom = null;
        
        setResizable(false);
        setTitle("Runtime Tests");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 349);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
        contentPane.setLayout(null);

        /* Labels! */
        lblTestList = new JLabel("Test List");
        lblTestList.setBounds(10, 11, 58, 23);
        contentPane.add(lblTestList);
        
        lblIPAddress = new JLabel("URL");
        lblIPAddress.setBounds(229, 12, 177, 20);
        contentPane.add(lblIPAddress);
        
        lblCredential = new JLabel("Credentials");
        lblCredential.setBounds(10, 158, 177, 20);
        contentPane.add(lblCredential);
        
        lblWebpages = new JLabel("Webpages");
        lblWebpages.setBounds(229, 52, 89, 14);
        contentPane.add(lblWebpages);
        
        lblForms = new JLabel("Forms");
        lblForms.setBounds(229, 136, 46, 14);
        contentPane.add(lblForms);

	    /* Buttons! */
        btnSave = new JButton("Save");
        btnSave.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		List_Active_Tests.clear();

        		String[] webpages = txtrPages.getText().split("\\s+");
				Enumeration<Form> forms = listFrm.elements();
        		
        		List_Active_Tests = listRunTests.getSelectedValuesList();
    			
    			if ( !List_Active_Tests.isEmpty() )
    			{
    				Web_Bypass webTestForFormTest = null;
        			for( Runtime_Web test : List_Active_Tests )
        			{
        				Runtime_Web rw = test.newCopy();
        				rw.set_Url(txtUrl.getText());
        				if( rw instanceof Web_Bypass ) {
        					rw.Add_To_Possibles(webpages);
        					if ( inputHitsFrom != null ) {
        						rw.setInputTest(new Webpage_Scan(ctlr, logging, inputHitsFrom.get_File()));
        					}
        					webTestForFormTest = (Web_Bypass) rw;
        				} else if (rw instanceof Runtime_Form ) {
        					while ( forms.hasMoreElements() ) {
        						rw.Add_To_Forms(forms.nextElement());
        					}
        					if ( inputHitsFrom != null ) {
	        					rw.setInputTest(new Post_Scan(ctlr, logging, inputHitsFrom.get_File()));
	        				}
        					if( webTestForFormTest != null ) {
        						Runtime_Form fb = (Runtime_Form)rw;
        						fb.setInputFromWebBypass(webTestForFormTest);
        					}
        				}
        				
	        			//logging.message( new Message( "Want to run: " + rw ) );
	        			ctlr.message(new Runtime_Tests_Message("Runtime Tests", rw));
	        		}
    			}
    			else
    			{
    				logging.message( new Message( "No Runtime Tests Selected" ) );
    			}
        	}
        });
	    btnSave.setBounds(229, 287, 106, 23);
	    contentPane.add(btnSave);

        btnExit = new JButton("Exit");
        btnExit.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		dispose();
        	}
        });
        btnExit.setBounds(335, 287, 99, 23);
        contentPane.add(btnExit);
        
        btnAddCred = new JButton("Add Credential");
        btnAddCred.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Credential_Dialog cd = new Credential_Dialog(getOwner());

        		cd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        		cd.setVisible(true);
        		
        		listCred.addElement(cd.getCredential());
        		
        		cd = null;
        	}
        });
        btnAddCred.setBounds(10, 287, 209, 23);
        contentPane.add(btnAddCred);
        
        /* Text Fields */
        txtUrl = new JTextField();
        txtUrl.setText("http://");
        txtUrl.setBounds(229, 28, 205, 20);
        contentPane.add(txtUrl);
        txtUrl.setColumns(10);
        
        /* Lists! */
        DefaultListModel<Runtime_Web> listTests = new DefaultListModel<Runtime_Web>();
        listTests.addElement(new Web_Bypass( ctlr, logging, null) );
        listTests.addElement(new Form_Bypass( ctlr, logging, null) );
        listTests.addElement(new URL_Overflow( ctlr, logging, null) );
        listTests.addElement(new Form_Overflow( ctlr, logging, null) );
        listTests.addElement(new Form_Validation( ctlr, logging, null) );
        listRunTests = new JList<Runtime_Web>(listTests);
        listRunTests.setToolTipText("Select multiple by using Control+Click");
        listRunTests.setBounds(10, 30, 209, 126);
        contentPane.add(listRunTests);
        
        scrollTests = new JScrollPane(listRunTests, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollTests.setBounds(10, 30, 209, 126);
        contentPane.add(scrollTests);

        listCred = new DefaultListModel<Credential>();
        listCredentials = new JList<Credential>(listCred);
        listCredentials.setBounds(10, 177, 209, 77);
        contentPane.add(listCredentials);
        
        scrollCred = new JScrollPane(listCredentials, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollCred.setBounds(10, 177, 209, 99);
        contentPane.add(scrollCred);
        
        /* Checkboxes */
        chckbxUseObjectCode = new JCheckBox("Use Object Code Output");
        chckbxUseObjectCode.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
                if ( chckbxUseObjectCode.isSelected() ) {
                	File_Dialog fd = new File_Dialog(getOwner(), ctlr);

                	fd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                	fd.setVisible(true);
                	
                	inputHitsFrom = fd.getBin_in();
                	
                	if( inputHitsFrom == null ) {
                		chckbxUseObjectCode.setSelected(false);
                	} else {
                		chckbxUseObjectCode.setSelected(true);
                		chckbxUseObjectCode.setText("Use "+inputHitsFrom.get_File().getName()+" Output");
                	}

                    fd = null;
                } else {
                	inputHitsFrom = null;
                	chckbxUseObjectCode.setText("Use Object Code Output");
                }
        	}
        });
        chckbxUseObjectCode.setBounds(229, 257, 205, 23);
        contentPane.add(chckbxUseObjectCode);
        
        /* Text area with Scrollbars */
        txtrPages = new JTextArea();
        txtrPages.setToolTipText("Enter each webpage on separate line");
        txtrPages.setBounds(228, 66, 206, 69);
        contentPane.add(txtrPages);
        
        scrollPages = new JScrollPane(txtrPages, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPages.setBounds(228, 66, 206, 69);
        contentPane.add(scrollPages);
        
        btnForm = new JButton("Add Form");
        btnForm.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Form_Dialog fd = new Form_Dialog(getOwner(), listCred);

            	fd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            	fd.setVisible(true);
        		
            	if(fd.getForm() != null) {
            		listFrm.addElement(fd.getForm());
            	}
        	}
        });
        btnForm.setBounds(229, 231, 205, 23);
        contentPane.add(btnForm);

        listForms = new JList<Form>(listFrm);
        
        scrollForm = new JScrollPane(listForms, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollForm.setBounds(229, 154, 205, 66);
        contentPane.add(scrollForm);
        
        debug_setup();
	}

	private void debug_setup() {
		
        txtUrl.setText("http://10.0.0.14/");
        
        //txtrPages.setText("index.htm\r\nlogin.htm\r\nnpmoddev.htm\r\nnpplot.htm\r\nnpreadings.htm\r\nsuper.htm\r\nsuplg.htm\r\nethupgrade.htm\r\nsensorconf.htm");
        txtrPages.setText("npmoddev.htm\r\nsuper.htm");
        
        listCred.addElement(new Credential("login", "0"));
        listCred.addElement(new Credential("admin", "0"));
        
	}
}
