package uSecured;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

public class Parser extends MicroSecured_Thread {
	
	public static final String THREAD_NAME = "Parser";

	public static final byte ASCII_LOW = 32; // ASCII SPACE
	// public static final byte ASCII_LOW = 33; // ASCII '!'
	public static final byte ASCII_HIGH = 126; // ASCII '~'
	public static final byte ASCII_CR = 10; // ASCII CR
	public static final byte ASCII_LF = 13; // ASCII LF

    private List<Analysis_Test> List_Tests = null;

    public Parser( int q_size, Controller c, Log l )
    {
    	setMsgQ( new ArrayBlockingQueue<Message>( q_size ) );
        set_Tests( new ArrayList<Analysis_Test>( ) );
        setLog(l);
        setController(c);

        start();
    }

    @Override
    public void run() {
    	StringBuilder str = new StringBuilder();
    	//msgLog("Parser Started");
        while ( isRunning() )
        {
			try {
				Message msg = recvMessage();
				
				switch ( msg.getType() ) {
    				case _Stop_Message:
    					if(str.length() > 0) {
                			// logging.message(new Message("Parse String Found: "+str.toString()));
                			send_To_Tests( new Message(str.toString()) );
                			str.setLength(0);
    					}
            			finish();
    					break;
    				case Bytes_Message:
    					Bytes_Message bmsg = (Bytes_Message)msg;
    		            for( byte b : bmsg.getData() ) {
    		            	// Printable chars including CR and LF
    		            	if( ( b >= ASCII_LOW  &&
    		            		  b <= ASCII_HIGH    ) ||
    		            	    b == ASCII_CR          ||
    		            	    b == ASCII_LF             ) {
    		            		str.append((char)b);
    		            	} else {
    		            		if(str.length() > 0) {
    		            			// logging.message(new Message("Parse String Found: "+str.toString()));
    		            			send_To_Tests( new Message(str.toString()) );
    		            			str.setLength(0);
    		            		}
    		            	}
    		            }
    					break;
    				case Parsed_File:
    					send_To_Tests(msg);
    					break;
    				default:
    					break;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
    }

    public List<Analysis_Test> get_Tests() {
        return List_Tests;
    }

    private void set_Tests(List<Analysis_Test> l) {
        List_Tests = l;
    }

	private void send_To_Tests( Message m ) {
        try {
        	for ( Analysis_Test a : this.List_Tests ) {
        		a.getMsgQ().put(m);
        	}
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void add_Test( Analysis_Test at ) {
        if ( List_Tests == null ) {
            set_Tests( new ArrayList<Analysis_Test>( ) );
        }

        List_Tests.add( at );
    }

	@Override
	public String get_Thread_Name() {
		return Parser.THREAD_NAME;
	}

	@Override
	public void finish() {
    	//msgLog(new Message("Parser Ended"));
    	send_To_Tests( new Stop_Message("Parsing is Complete") );
        super.finish();
	}

	@Override
	public String toString() {
		return Parser.THREAD_NAME;
	}

	@Override
	public Parser newCopy() {
		return new Parser(getMsgQ().size(), getController(), getLog());
	}

}
