package uSecured;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class File_Dialog extends JDialog {

	private static final long serialVersionUID = 818076938092449898L;
	
	private Bin_Input bin_in = null;
	
	private final JPanel contentPanel = new JPanel();
	private JComboBox<Bin_Input> selectBin;
	private JLabel lblWarning;

	// public static void main(String[] args) {
	// 	try {
	// 		File_Dialog dialog = new File_Dialog();
	// 		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	// 		dialog.setVisible(true);
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 	}
	// }

	/**
	 * Create the dialog.
	 */
	public File_Dialog(Window own, Controller ctlr) {
		super(own, "Object Code Output From:", Dialog.ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 319, 109);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			lblWarning = new JLabel("Must add Object Code tests first!");
			lblWarning.setVisible(false);
			contentPanel.add(lblWarning);
		}
		{
			List<Bin_Input> binList = ctlr.get_List_Files();
			selectBin = new JComboBox<Bin_Input>();
			if ( !binList.isEmpty() ) {
				for( Bin_Input b : binList ) {
					selectBin.addItem(b);
				}
				selectBin.setSelectedIndex(0);
				selectBin.setVisible(true);
				lblWarning.setVisible(false);
			}
			else{
				selectBin.setVisible(false);
				lblWarning.setVisible(true);
			}
			contentPanel.add(selectBin);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Select File");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(selectBin.getSelectedIndex() > -1) {
							bin_in = (Bin_Input) selectBin.getSelectedItem();
							dispose();
						}
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						bin_in = null;
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

	public Bin_Input getBin_in() {
		return bin_in;
	}

}
