package uSecured;

import java.util.concurrent.BlockingQueue;

public class No_Decoder extends Decoder {
	
	public static final int MIN_DECODE_SIZE = 1;
	public static final boolean ON_THE_FLY = false;

	public No_Decoder(int q_in_size, BlockingQueue<Message> q_out,
			Controller c, Log l) {
		super(q_in_size, q_out, c, l);
	}

	@Override
	public byte[] Decode(byte[] in_bytes) {
		return in_bytes;
	}

	@Override
	public byte min_size_needed() {
		return MIN_DECODE_SIZE;
	}

	@Override
	public boolean b_On_The_Fly_Possible() {
		return false;
	}

	@Override
	public String toString() {
		return "None";
	}

	@Override
	public No_Decoder newCopy() {
		return new No_Decoder(1, get_Q_Out(), getController(), getLog());
	}

}
