package uSecured;

import java.awt.Dialog;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JTextArea;

public class Log_Dialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6369550495264974580L;
	private JScrollPane scrollPaneLog;
	private JTextArea textAreaLog;

	/**
	 * Create the dialog.
	 */
	public Log_Dialog(Window own, Controller ctlr, Log log) {
		super(own, "Log Buffer", Dialog.ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 600, 500);
		
		scrollPaneLog = new JScrollPane();
		getContentPane().add(scrollPaneLog, BorderLayout.CENTER);
		
		textAreaLog = new JTextArea();
		scrollPaneLog.setViewportView(textAreaLog);
		
		textAreaLog.setText(log.getLogBuffer());

	}

}
