package uSecured;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Zip_Input extends Bin_Input {
    
    public static final String THREAD_NAME = "Zip Input";
	
	private ZipFile zip = null;
	private Enumeration<? extends ZipEntry> files = null;

	public Zip_Input(File f, Controller c, Log l) {
		super(f, c, l);
		if( get_File() != null ) {
			set_Zip_Files();
		}
	}

	@Override
	protected InputStream get_Input_Stream() throws IOException {
		ZipEntry zentry = files.nextElement();
		while(zentry.isDirectory()&&files.hasMoreElements()) {
			zentry = files.nextElement();
		}
		if(!zentry.isDirectory()) {
			return zip.getInputStream(zentry);			
		} else {
			throw new IOException("no more files in the zip file");
		}
	}

	@Override
	public void finish() {
		if ( !files.hasMoreElements() ) {
			try {
				zip.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			super.finish();
		}
	}

	@Override
	public void set_File(File f) {
		super.set_File(f);
		if( get_File() != null ) {
			set_Zip_Files();
		}
	}

	@Override
	public String get_Thread_Name() {
        return Zip_Input.THREAD_NAME;
	}

	public ZipFile getZip() {
		return zip;
	}

	private void setZip(ZipFile zip) {
		this.zip = zip;
	}

	public Enumeration<? extends ZipEntry> getFiles() {
		return files;
	}

	private void setFiles(Enumeration<? extends ZipEntry> files) {
		this.files = files;
	}

	private void set_Zip_Files() {
		try {
			setZip(new ZipFile(get_File()));
			setFiles(zip.entries());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Zip_Input newCopy(File f) {
		return new Zip_Input(f, getController(), getLog());
	}

}
