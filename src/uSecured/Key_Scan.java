package uSecured;

import java.io.File;
import java.nio.charset.StandardCharsets;

//import javax.swing.JOptionPane;

public class Key_Scan extends Obj_Code {
	
	private Histogram byte_values;

	public Key_Scan( Controller c, Log l, File f) {
		super( c, l, f );
	}

	@Override
	public String toString()
	{
		return super.toString() + " Key Scan";
	}

	@Override
    public boolean b_Check_String( String msg ) {
		
		if( msg.length() < 32 ) {
			return false;
		}

		String lower = new String(msg);
		// splitting on any number of spaces right now?
		// change to '+'?
		// change to "( |\t)+" ?
		String[] splits = msg.trim().split(" *");
		
		for(String split : splits ) {
			byte_values = new Histogram( getLog() );
			byte[] bytes = split.getBytes(StandardCharsets.US_ASCII);
			
			byte_values.Add_To_Histogram(bytes, bytes.length);
			byte_values.set_String_Histogram();
			byte_values.calculate();
			
			// DESIGN: check the mean, min, max, span, std_dev, and variance to decide
			// whether to return true or false
	
			// msgLog(new Message("Key_Scan for "+ msg));
			// msgLog(new Message("Key_Scan min: "+ byte_values.get_Min()));
			// msgLog(new Message("Key_Scan max: "+ byte_values.get_Max()));
			// msgLog(new Message("Key_Scan span: "+ byte_values.get_Span()));
			// msgLog(new Message("Key_Scan mean: "+ byte_values.get_Mean()));
			// msgLog(new Message("Key_Scan variance: "+ byte_values.get_Variance()));
			// msgLog(new Message("Key_Scan std dev: "+ byte_values.get_Standard_Deviation()));
			
			// JOptionPane.showMessageDialog(null, msg, "Key_Scan b_Check_String", 
			// 		JOptionPane.INFORMATION_MESSAGE );
			
			if ( byte_values.get_Mean() > 1 && byte_values.get_Standard_Deviation() > 4 ) {
				// msgLog(new Message("Key_Scan ret true for: "+ msg));
				return true;
			}
		}
		
		if( 	lower.toLowerCase().contains("rsa") ||
				lower.toLowerCase().contains("key") ||
				lower.toLowerCase().contains("certificate") ) {
			// msgLog(new Message("Key_Scan ret true for: "+ msg));
			return true;
		}
    	
		return false;
    }

	@Override
	public Key_Scan newCopy( File newFile ) {
		return new Key_Scan(getController(), getLog(), newFile);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((byte_values == null) ? 0 : byte_values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Key_Scan)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

}
