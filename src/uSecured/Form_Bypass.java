package uSecured;

public class Form_Bypass extends Runtime_Form {

	public Form_Bypass(Controller c, Log l, String _url_) {
		super(c, l, _url_);
	}

	@Override
	public Form_Bypass newCopy(String url) {
		return new Form_Bypass(getController(), getLog(), url);
	}

	@Override
	public String toString() {
		return super.toString() + " Authentication Bypass";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Form_Bypass)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

}
