package uSecured;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Form_Validation extends Runtime_Form {
	
	// Include CR and LF?
	public static final String INVALID_CHARS = ";+-<>()[]'\"&";
	// not including '%' since it is most likely handled as an HTML code

	public Form_Validation(Controller c, Log l, String _url_) {
		super(c, l, _url_);
		Add_Authorization(true);
	}

	@Override
	public Form_Validation newCopy(String url) {
		return new Form_Validation(getController(), getLog(), get_Url());
	}

	@Override
	public String toString() {
		return super.toString() + " Input Validation";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Form_Validation)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

	@Override
	protected String printFormItem(FormItem formItem, boolean printAmpersand) {
		if(formItem.Type().equals("text")){
			return formItem.toFormSubmitString(printAmpersand, INVALID_CHARS);
		}
		return formItem.toFormSubmitString(printAmpersand);
	}
	
	// Check for a good response - ret true means extraHTMLParsing is called
	@Override
	protected boolean checkResponse(URL u, HttpURLConnection conn)
			throws IOException {
		return ( conn.getResponseCode() / 100  == 2 );
	}
	
	// load back the page that has the form on it and check for the bad characters
	@Override
	protected void extraHTMLParsing(BufferedReader reader, URL u) {
		// reload the main page to see if the unit is still responding
		URL baseURL = null;
		HttpURLConnection conn = null;
		BufferedReader readerParentURL = null;
		boolean b_Same_Form_Read_Back = false;
		try {
			// trying reading in the page...maybe the form is already in there...
			b_Same_Form_Read_Back = parseFormForInvalidChars(reader, u);

			if( !b_Same_Form_Read_Back ) {
				// try reading the parent URL associated with the form to check for changed items including INVALID_CHARS
				baseURL = new URL( getObj_Code_Form_Current().FromURL() );
				conn = (HttpURLConnection) baseURL.openConnection();
				conn.setReadTimeout(2*1000);
				conn.setConnectTimeout(2*1000);
				conn.connect();
				readerParentURL = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				b_Same_Form_Read_Back = parseFormForInvalidChars(readerParentURL, u);
			}
		} catch (MalformedURLException e) {
			msgLog("Form " + getObj_Code_Form_Current().Action() + " has no parent webpage listed. Changed items cannot be validated.");
			e.printStackTrace();
		} catch ( SocketTimeoutException se ) {
			msgLog("Page ("+u.toExternalForm()+") Response Timed Out waiting to check for changed form items including: "+INVALID_CHARS);
			// se.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if ( readerParentURL != null ) {
				try {
					readerParentURL.close();
				} catch (IOException e) {
					// intentionally ignore this
				}
				readerParentURL = null;
			}
			if ( conn != null ) {
				conn.disconnect();
				conn = null;
			}
		}
	}

	private boolean parseFormForInvalidChars(BufferedReader reader, URL u) {
		String webpage = Runtime_Web.readWebpage( reader );
		Document page = Jsoup.parse(webpage);
		Elements forms = page.body().select("form");
		StringBuilder badCharsAllowed = new StringBuilder();
		for (Element f : forms){
			Form form = Form.createForm( f );

			if( form.equals( getObj_Code_Form_Current() ) ) {
				for( FormItem fi : form.getFormItems()) {
					if ( fi.Type().equals("text") ) {
						for( int i = 0 ; i < INVALID_CHARS.length() ; i++ ){
							if ( fi.Value().contains( INVALID_CHARS.substring(i, i+1) ) ) {
								badCharsAllowed.append(INVALID_CHARS.substring(i, i+1));
							}
						}
						if( badCharsAllowed.length() > 0 ){
							Add_To_List(u.toExternalForm()+": input "+fi.Name()+" accepts the following dangerous characters: "+badCharsAllowed.toString());
							badCharsAllowed.setLength(0);
						}
					}
				}
				return true;
			}
		}
		return false;
	}

}
