package uSecured;

import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

public class FormItemElement extends Element {
	
	private FormItemType type = FormItemType.Not_Set;

	public FormItemElement(Tag tag, String baseUri) {
		super(tag, baseUri);
	}

	public FormItemElement(Tag tag, String baseUri, Attributes attributes) {
		super(tag, baseUri, attributes);
	}
	
	public String itemName() {
		return super.attr("name");
	}
	
	public FormItemElement itemName(String name) {
		return (FormItemElement) super.attr("name", name);
	}
	
	public String itemValue() {
		return super.attr("value");
	}
	
	public FormItemElement itemValue(String name) {
		return (FormItemElement) super.attr("value", name);
	}

	public String Type() {
		if( type == FormItemType.Not_Set ) {
			switch(super.tagName().toLowerCase()){
			case "input" :
				Type(FormItemType.Input);
				break;
			case "select" :
				Type(FormItemType.Select);
				break;
			case "textarea" :
				Type(FormItemType.TextArea);
				break;
			default :
				Type(FormItemType.Unhandled_Type);
				break;
			}
		}
		if ( type == FormItemType.Unhandled_Type ) {
			return super.tagName();
		}
		return type.toString();
	}

	public void Type(FormItemType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return Type() + " " + itemName() + " = " + itemValue();
	}

}
