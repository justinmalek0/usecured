package uSecured;

import java.io.File;

public abstract class Obj_Code extends Analysis_Test {
    private File file;

	public Obj_Code( Controller c, Log l, File f ) {
        super( c, l );
        set_File( f );
    }
	
	public abstract Obj_Code newCopy( File newFile );

    @Override
	public Analysis_Test newCopy() {
		return newCopy(get_File());
	}
    

	@Override
    public void run( ) {
    	//msgLog(new Message("Obj_Code Started"));
        while ( isRunning() )
        {
	        try {
				Message msg = getMsgQ().take();
				
				if(msg instanceof Stop_Message) {
					finish();
				} else if ( b_Check_String(msg.getMsg()) )
				{
					Add_To_List( msg.getMsg() );
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
    }
    
    @Override
	public void finish( ) {
    	//msgLog(new Message("Obj_Code Ended"));
        super.finish();
    }
    
    public abstract boolean b_Check_String( String msg );

    public File get_File() {
        return file;
    }

    public void set_File(File f) {
    	file = f;
    }

	@Override
	public String toString() {
		if ( this.get_File() == null )
		{
			return "Object Code";
		}
		
		if(isComplete()) {
			return "Completed: " + this.get_File().getName() + ": Object Code";
		}

		return this.get_File().getName() + ": Object Code";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Obj_Code)) {
			return false;
		}
		Obj_Code other = (Obj_Code) obj;
		if (file == null) {
			if (other.file != null) {
				return false;
			}
		} else if (!file.equals(other.file)) {
			return false;
		}
		return true;
	}
}
