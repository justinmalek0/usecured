package uSecured;

public class FormItem {
	
	private String name = null;
	private String value = null;
	private String type = null;

	public FormItem(String n, String v) {
		Name(n);
		Value(v);
	}
	
	public FormItem() { }

	public String Name() {
		return name;
	}

	public FormItem Name(String name) {
		this.name = new String( name );
		return this;
	}

	public String Value() {
		return value;
	}

	public FormItem Value(String value) {
		this.value = new String( value );
		return this;
	}

	public String Type() {
		if ( type == null ) {
			return "";
		}
		return type;
	}

	public FormItem Type(String t) {
		this.type = new String( t );
		return this;
	}

	@Override
	public String toString() {
		return name + "=" + value;
	}

	public String toFormSubmitString(boolean printAmpersand) {
		if ( !Type().equals("reset") ) {
			if ( printAmpersand ) {
				return "&"+toString();
			}
			return toString();
		}
		return "";
	}

	public String toFormSubmitString(boolean printAmpersand, String newValue) {
		if ( !Type().equals("reset") ) {
			if ( printAmpersand ) {
				return "&"+Name()+"="+newValue;
			}
			return Name()+"="+newValue;
		}
		return "";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormItem other = (FormItem) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		//if (value == null) {
		//	if (other.value != null)
		//		return false;
		//} else if (!value.equals(other.value))
		//	return false;
		return true;
	}

}
