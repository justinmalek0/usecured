package uSecured;

public class Thread_Complete_Message extends Message {
	
	private final MicroSecured_Thread ms_thread;

	public Thread_Complete_Message(String m, MicroSecured_Thread t) {
		super(Message_Type.Thread_Complete,m);
		ms_thread = t;
	}

	public MicroSecured_Thread get_MS_Thread() {
		return ms_thread;
	}

}
