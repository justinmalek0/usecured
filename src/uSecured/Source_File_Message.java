package uSecured;

public class Source_File_Message extends Message {
	
	private Source_Input sourceInput;

	public Source_File_Message( String m, Source_Input si ) {
		super(Message_Type.Source_File, m);
		setSourceInput(si);
	}

	public Source_Input getSourceInput() {
		return sourceInput;
	}

	public void setSourceInput(Source_Input s) {
		this.sourceInput = s;
	}

}
