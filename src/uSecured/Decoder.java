package uSecured;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public abstract class Decoder extends MicroSecured_Thread {
	
	public static final String THREAD_NAME = "Decoder";
    
    private BlockingQueue<Message> Q_Output;

    public Decoder( int q_in_size, BlockingQueue<Message> q_out, Controller c, Log l ) {
        setController( c );
        setLog(l);
    	setMsgQ( new ArrayBlockingQueue<Message>( q_in_size ) );
    	set_Q_Out( q_out );
    }

	public abstract byte[ ] Decode( byte[ ] in_bytes ) throws Exception;
    public abstract byte min_size_needed( );
    public abstract boolean b_On_The_Fly_Possible( );
    @Override
    public abstract String toString( );
	@Override
	public abstract Decoder newCopy();
	

	@Override
	public void run() {
		byte[] bytes_to_decode = null;
		byte[] leftover_bytes = null;
		ByteArrayOutputStream buffering_bytes = null;
		int buffering_idx = 0;
		if ( b_On_The_Fly_Possible() ) {
			bytes_to_decode = new byte[ min_size_needed() ];
		} else {
			buffering_bytes = new ByteArrayOutputStream();
		}

    	//msgLog(new Message("Decoder Started"));
		
		while( isRunning() ) {
			try {
				Message msg = recvMessage();
				
				if ( msg instanceof Stop_Message ) {
					// finish decoding/uncompressing if necessary - send to Q_Output
					if ( !b_On_The_Fly_Possible() ) {
						bytes_to_decode = buffering_bytes.toByteArray();
			        	try {
							get_Q_Out().put(new Bytes_Message("Data Converted", Decode( bytes_to_decode )));
						} catch (Exception e) {
							// Basically want to do nothing and continue
						}
					}
					finish();
				} else if ( msg instanceof Bytes_Message ) {
					Bytes_Message bmsg = (Bytes_Message)msg;
					// msgLog(new Message("Decoder bmsg.getData: "+bmsg.getData()));
		        	
		        	// for( byte b : bmsg.getData()) {
		        	// 	msgLog(new Message("Decoder data: " + b));	
		        	// }

		        	String str = new String( bmsg.getData() );
		        	// msgLog(new Message("Decoder b4: "+str));
		        	// REMOVE THE CR/LF in bmsg.getData()...
		        	str = str.replaceAll("\n|\r", "");
		        	// msgLog(new Message("Decoder after: "+str));
		        	byte[] data = str.getBytes(StandardCharsets.US_ASCII);
					
					// decode/uncompress and send if possible to do at once
					// else buffer up the bytes to be uncompressed at the end
					if ( b_On_The_Fly_Possible() ) {
						int i;
						
						if ( leftover_bytes == null ) {
							// msgLog(new Message("Decoder i set to 0"));
							i = 0;
						} else {
							i = bytes_to_decode.length - leftover_bytes.length;
							// msgLog(new Message("Decoder i set to "+i));
							System.arraycopy(leftover_bytes, 0, bytes_to_decode, 0, leftover_bytes.length);
							System.arraycopy(data, 0, bytes_to_decode, leftover_bytes.length, i);
							leftover_bytes = null;
				        	try {
								get_Q_Out().put(new Bytes_Message("Data Converted", Decode( bytes_to_decode )));
							} catch (Exception e) {
								// Basically want to do nothing and continue
							}
						}
						
						// len - 1 because I am checking addressable indexes here...
						for (  ; i + bytes_to_decode.length - 1 < data.length ; i += bytes_to_decode.length ) {
							// msgLog(new Message("Decoder i is now: "+i));
							System.arraycopy(data, i, bytes_to_decode, 0, bytes_to_decode.length );
				        	// for( byte b : bytes_to_decode) {
				        	// 	msgLog(new Message("Decoder bytes_to_decode: " + b));	
				        	// }
							//msgLog(new Message("Decoder bytes_to_decode: "+bytes_to_decode));
				        	try {
								get_Q_Out().put(new Bytes_Message("Data Converted", Decode( bytes_to_decode )));
							} catch (Exception e) {
								// Basically want to do nothing and continue
							}
						}
						
						if ( i < data.length ) {
							leftover_bytes = new byte [ data.length - i ];

							System.arraycopy(data, i, leftover_bytes, 0, leftover_bytes.length );
						}
					} else {
						// On the fly converting is not possible
						buffering_bytes.write( data, buffering_idx, data.length );
						buffering_idx += data.length;
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void finish() {
    	try {
			get_Q_Out().put(new Stop_Message("Decoder is Complete"));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		super.finish();
	}
	
	@Override
	public String get_Thread_Name() {
		return Decoder.THREAD_NAME;
	}

	public BlockingQueue<Message> get_Q_Out() {
		return Q_Output;
	}

	public void set_Q_Out(BlockingQueue<Message> q_out) {
		Q_Output = q_out;
	}

}
