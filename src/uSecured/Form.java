package uSecured;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Form {

	public static final String MACRO_SEPARATOR = "~";
	public static final String MICRO_SEPARATOR = "`";
	
	private String action = null;
	private String name = null;
	private String method = null;
	private String encode = null;
	private String fromURL = null;
	private Credential credential = null;
	private List<FormItem> formItems = null;
	
	public Form( String m, String n, String a ) {
		action = a;
		name = n;
		method = m;
		setFormItems(new ArrayList<FormItem>());
	}
	
	public Form( String m, String a ) {
		method = m;
		action = a;
		setFormItems(new ArrayList<FormItem>());
	}

	public String Action() {
		return action;
	}

	public Form Action( String a ){
		action = a;
		return this;
	}

	public String Name() {
		return name;
	}

	public Form Name( String n ){
		name = n;
		return this;
	}

	public String Method() {
		return method;
	}

	public Form Method( String m ){
		method = m;
		return this;
	}

	public String Encode() {
		return encode;
	}

	public Form Encode( String e ){
		encode = e;
		return this;
	}

	public String FromURL() {
		return fromURL;
	}

	public Form FromURL( String url ){
		fromURL = url;
		return this;
	}

	public Credential Credential() {
		return credential;
	}

	public Form Credential(Credential c) {
		this.credential = c;
		return this;
	}

	@Override
	public String toString(){
		if(name == null){
			return action + " " + method + " request";
		}
		return name + ": " + action + " " + method + " request";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Form other = (Form) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		// if (name == null) {
		// 	if (other.name != null)
		// 		return false;
		// } else if (!name.equals(other.name))
		// 	return false;
		return true;
	}

	public List<FormItem> getFormItems() {
		return formItems;
	}

	public void setFormItems(List<FormItem> formItems) {
		this.formItems = formItems;
	}
	
	public boolean Add_Form_Item( FormItem i ) {
        for( FormItem f : formItems )
        {
            if ( f.equals( i ) )
            {
                return false;
            }
        }
		formItems.add(i);
		
		return true;
	}
    
    public boolean Add_Form_Items( ArrayList<FormItem> List )
    {
        for( FormItem f : List )
        {
        	Add_Form_Item( f );
        }
        
        return true;
    }
    
    public boolean Add_Form_Items( FormItem[] List )
    {
        for( FormItem f : List )
        {
        	Add_Form_Item( f );
        }
        
        return true;
    }
    
    public String PrintAll( ) {
    	StringBuilder str = new StringBuilder();

    	if( method != null && method.length() > 0 ){
    		str.append(method);
    	} else {
    		str.append(" ");
    	}
    	str.append(MICRO_SEPARATOR);
    	if( name != null && name.length() > 0 ){
    		str.append(name);
    	} else {
    		str.append(" ");
    	}
    	str.append(MICRO_SEPARATOR);
    	if( action != null && action.length() > 0 ){
    		str.append(action);
    	} else {
    		str.append(" ");
    	}
    	str.append(MICRO_SEPARATOR);
    	if( encode != null && encode.length() > 0 ){
    		str.append(encode);
    	} else {
    		str.append(" ");
    	}
    	str.append(MICRO_SEPARATOR);
    	if( fromURL != null && fromURL.length() > 0 ){
    		str.append(fromURL);
    	} else {
    		str.append(" ");
    	}
    	str.append(MICRO_SEPARATOR);
    	if( credential != null ){
    		str.append(credential.getUsername()+MICRO_SEPARATOR+credential.getPassword());
    	} else {
    		str.append(" "+MICRO_SEPARATOR+" ");
    	}
    	
    	if ( formItems != null && !formItems.isEmpty() ){
    		for ( FormItem i : formItems ) {
    			str.append(MACRO_SEPARATOR+i.Name());
    			if(i.Name().length() == 0) {
    				str.append("noname");
    			}
                str.append(MICRO_SEPARATOR+i.Value());
                if(i.Value().length() == 0) {
                    str.append("novalue");
                }
    			str.append(MICRO_SEPARATOR+i.Type());
    			if(i.Type().length() == 0) {
    				str.append(" ");
    			}
    		}
    	}
    	
    	return str.toString();
    }
    
    public static Form FormFromPrintAll(String printall) {
    	String[] macros = printall.split(MACRO_SEPARATOR);
    	
    	if(macros.length > 0) {
    		String[] micros = macros[0].split(MICRO_SEPARATOR);
    		Form frm = new Form(micros[0],micros[1],micros[2]);
    		if( micros[3] != null && !micros[3].equals(" ") ) {
    			frm.Encode(micros[3]);
    		}
    		if( micros[4] != null && !micros[4].equals(" ") ) {
    			frm.FromURL(micros[4]);
    		}
    		if( micros[5] != null && !micros[5].equals(" ") && micros[6] != null && !micros[6].equals(" ") ) {
    			frm.Credential(new Credential(micros[5], micros[6]));
    		}
    		for( int i = 1 ; i < macros.length ; i++ ){
    			micros = macros[i].split(MICRO_SEPARATOR);
                FormItem itemToAdd = new FormItem(micros[0],micros[1]);
                if( micros[2] != null && !micros[2].equals(" ") ) {
                    itemToAdd.Type(micros[2]);
                }
                frm.Add_Form_Item(itemToAdd);
    		}
    		return frm;
    	}
    	
    	return new Form("","","");
    }

    public static Form createForm(Element form){
    	Form frm = new Form(form.attr("method").toLowerCase(), form.attr("name"), form.attr("action"));
		if( form.hasAttr("enctype") ) {
			frm.Encode(form.attr("enctype"));
		}
		// msgLog("FOUND: "+form.attr("method")+" Form named "+form.attr("name")+" w action "+form.attr("action"));
		Elements items = form.select("input, select");
		for( Element item : items ) {
			FormItem toAdd;
			switch( item.tagName().toLowerCase() ){
			case "input" :
				toAdd = new FormItem(item.attr("name"), item.attr("value"));
				toAdd.Type( item.attr("type").toLowerCase() );
				frm.Add_Form_Item(toAdd);
				// msgLog("FOUND: "+item.tagName()+ " [type=" + item.attr("type") + "] & [name=" + item.attr("name") + "] & [value="+ item.attr("value")+"]");
				break;
			case "select":
				Elements options = item.children().select("option");
				StringBuilder optPrint = new StringBuilder();
				for(Element opt:options){
					optPrint.append("\r\n\t[value="+opt.attr("value")+"]");
					if(opt.hasAttr("selected")) {
						optPrint.append(" [selected]");
						toAdd = new FormItem(item.attr("name"), item.attr("value"));
						toAdd.Type( "select" );
						frm.Add_Form_Item(toAdd);
					}
				}
				// msgLog( "FOUND: "+item.tagName()+ " [name=" + item.attr("name") + "] w/ options:"+optPrint.toString());
				break;
			default :
				break;
			}
		}
		return frm;
    }

}
