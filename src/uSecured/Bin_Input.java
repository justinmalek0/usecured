package uSecured;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Bin_Input extends MicroSecured_Thread {
    
    public static final String THREAD_NAME = "Bin Input";
    
    private final int READ_BUFFER_SIZE = 1024;

    private Histogram byte_values;
    private File file;
    private Decoder decoder;
    
    public Bin_Input( File f, Controller c, Log l )
    {
        this.file = f;
        byte_values = new Histogram( l );
        setLog(l);
        setController(c);
        set_Decoder(null);
    }

    @Override
    public void run() {
        InputStream in;
        //msgLog(new Message(get_Thread_Name()+" Started"));
        if ( decoder != null ) {
            decoder.set_Q_Out( getMsgQ() );
            setMsgQ( decoder.getMsgQ() );
            decoder.start();
        }
        while( isRunning() ) {
            try {
                in = get_Input_Stream();
                
                process_Input_Stream(in);
                
                in.close();
                
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finish();
        }
    }
    
    protected InputStream get_Input_Stream( ) throws FileNotFoundException, IOException {
        return new BufferedInputStream(new FileInputStream(file));
    }
    
    protected void process_Input_Stream( InputStream in ) throws InterruptedException {
        @SuppressWarnings("unused")
		int total_bytes_read = 0;
        byte[] data_buffer = new byte[ READ_BUFFER_SIZE ];
        ByteArrayOutputStream buffering_bytes = new ByteArrayOutputStream();
        
        int bytes_read;
        byte[] data;
        try {
			while( ( bytes_read = in.read(data_buffer) ) != -1 ) {
			    buffering_bytes.write(data_buffer, 0, bytes_read);
			    
			    total_bytes_read += bytes_read;
			    
			    data = buffering_bytes.toByteArray();
			    byte_values.Add_To_Histogram(data, bytes_read);
			    getMsgQ().put(new Bytes_Message(get_Thread_Name()+" Size " + bytes_read, data));
			    
			    buffering_bytes.reset();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        // while ( total_bytes_read < file.length())
        // {
        //     int bytes_read = 0;
            
        //     if ( file.length() - total_bytes_read > READ_BUFFER_SIZE ) {
        //         data = new byte[ READ_BUFFER_SIZE ];
        //     } else {
        //         data = new byte[ (int) (file.length() - total_bytes_read) ];
        //     }
            
        //     while ( bytes_read < data.length && ((total_bytes_read + bytes_read) < file.length()) ) {
        //         int bytes_remaining = data.length - bytes_read;
        //         int count;
        //         try {
        //             count = in.read(data, bytes_read, bytes_remaining);
        //             // msgLog(new Message("Bin_Input in.read ret: "+count));
        //             if (count > 0){
        //                 bytes_read += count;
        //             } else if (count < 0) {
        //                 break;
        //             }
        //         } catch (IOException e) {
        //             e.printStackTrace();
        //         }
        //     }
        //     total_bytes_read += bytes_read;
            
        //     // for( byte b : data) {
        //     //  msgLog(new Message("Bin Read: " + b));  
        //     // }
        //     // msgLog(new Message("Read "+bytes_read+"!"));
        //     byte_values.Add_To_Histogram(data, bytes_read);
            
        //     getMsgQ().put(new Bytes_Message("Bin Input Size " + bytes_read, data));
        // }
    }

    public File get_File() {
        return file;
    }

    public void set_File(File f) {
        file = f;
    }

    public Decoder get_Decoder() {
        return decoder;
    }

    public void set_Decoder(Decoder d) {
        this.decoder = d;
    }

    @Override
    public String get_Thread_Name() {
        return Bin_Input.THREAD_NAME;
    }

    @Override
    public void finish() {
        try {
            getMsgQ().put(new Stop_Message(get_Thread_Name()+" Reading is Complete"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //msgLog(new Message(get_Thread_Name()+" Ended"));
        super.finish();
    }

    @Override
    public String toString() {
        if ( file == null ) {
            return get_Thread_Name();
        }
        return file.getName()+" "+get_Thread_Name();
    }
    
    public Bin_Input newCopy( File f ) {
    	return new Bin_Input( f, getController(), getLog() );
    }
    
    @Override
    public Bin_Input newCopy( ) {
    	return newCopy( get_File() );
    }

}
