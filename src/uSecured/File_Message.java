package uSecured;

import java.io.File;

public class File_Message extends Message {
	private File file;
	private Decoder decoder;
	private Bin_Input input;
	
	public File_Message( String m, File f, Decoder d, Bin_Input bi) {
		super(Message_Type.Obj_File, m);
		set_File( f );
		set_Decoder(d);
		setInput(bi);
	}

	public File get_File() {
		return file;
	}

	public void set_File(File file) {
		this.file = file;
	}

	public Decoder get_Decoder() {
		return decoder;
	}

	public void set_Decoder(Decoder decoder) {
		this.decoder = decoder;
	}

	public Bin_Input getInput() {
		return input;
	}

	public void setInput(Bin_Input input) {
		this.input = input;
	}

}
