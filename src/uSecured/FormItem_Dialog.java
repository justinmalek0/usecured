package uSecured;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;

public class FormItem_Dialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2795955149468699668L;
	
	private FormItem item = null;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtKey;
	private JTextField txtValue;

	/**
	 * Create the dialog.
	 */
	public FormItem_Dialog(Window own) {
		super(own, "New Web Form", Dialog.ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 262, 126);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblKeyName = new JLabel("Key Name");
			GridBagConstraints gbc_lblKeyName = new GridBagConstraints();
			gbc_lblKeyName.anchor = GridBagConstraints.EAST;
			gbc_lblKeyName.insets = new Insets(0, 0, 5, 5);
			gbc_lblKeyName.gridx = 0;
			gbc_lblKeyName.gridy = 0;
			contentPanel.add(lblKeyName, gbc_lblKeyName);
		}
		{
			txtKey = new JTextField();
			GridBagConstraints gbc_txtKey = new GridBagConstraints();
			gbc_txtKey.insets = new Insets(0, 0, 5, 0);
			gbc_txtKey.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtKey.gridx = 1;
			gbc_txtKey.gridy = 0;
			contentPanel.add(txtKey, gbc_txtKey);
			txtKey.setColumns(10);
		}
		{
			JLabel lblValue = new JLabel("Value");
			GridBagConstraints gbc_lblValue = new GridBagConstraints();
			gbc_lblValue.anchor = GridBagConstraints.EAST;
			gbc_lblValue.insets = new Insets(0, 0, 0, 5);
			gbc_lblValue.gridx = 0;
			gbc_lblValue.gridy = 1;
			contentPanel.add(lblValue, gbc_lblValue);
		}
		{
			txtValue = new JTextField();
			GridBagConstraints gbc_txtValue = new GridBagConstraints();
			gbc_txtValue.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtValue.gridx = 1;
			gbc_txtValue.gridy = 1;
			contentPanel.add(txtValue, gbc_txtValue);
			txtValue.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Add Form Item");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if( !txtKey.getText().isEmpty() && !txtValue.getText().isEmpty() ) {
							item = new FormItem(txtKey.getText(), txtValue.getText());
						} else {
							item = null;
						}
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						item = null;
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}
	
	public FormItem getItem() {
		return item;
	}

}
