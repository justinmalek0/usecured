package uSecured;

import java.util.Arrays;

public class Histogram {
	
	public final int IDX_MAX = 0xFF; 
	public final int IDX_MIN = 0; 

	@SuppressWarnings("unused")
	private Log logging;
	private int[] histogram;
	private int idx_low = IDX_MIN;
	private int idx_high = IDX_MAX;
	private int min = Integer.MAX_VALUE;
	private int max = Integer.MIN_VALUE;
	private double mean = 0;
	private double sum = 0;
	private double std_dev = 0;
	private double variance = 0;
	
	public Histogram( Log l ) {
		histogram = new int[IDX_MAX + 1];
		logging = l;
	}
	
	public int get_Idx_high() {
		return idx_high;
	}

	public void set_Idx_high(int high) {
		if ( high > idx_low && high <= IDX_MAX ) {
			idx_high = high;
		}
	}

	public int get_Idx_low() {
		return idx_low;
	}

	public void set_Idx_low(int low) {
		if ( low < idx_high && low >= IDX_MIN ) {
			idx_low = low;
		}
	}

	public void Add_To_Histogram( byte[] in, int size ) {
		for ( int i = 0 ; i < size ; i++  )
		{
			if (in[i] < 0 ) {
				histogram[ in[i]*-1 ]++;
			} else {
				histogram[ in[i] ]++;
			}
		}
	}
	
	public int[] get_Histogram( ) {
		return histogram;
	}
	
	public void calculate( ) {
		for ( int i = idx_low; i <= idx_high; i++ ) {
			if ( histogram[i] < min ) {
				min = histogram[i];
			}
			if ( histogram[i] > max ) {
				max = histogram[i];
			}
			sum += histogram[i];
		}
		
		mean = sum / histogram.length;

		for ( int i = idx_low; i <= idx_high; i++ ) {
			variance += Math.pow(histogram[i] - mean, 2) / histogram.length;
		}
		
		std_dev = Math.sqrt(variance);
	}
	
	public int get_Min( ) {
		return min;
	}
	
	public int get_Max( ) {
		return max;
	}
	
	public double get_Mean( ) {
		return mean;
	}
	
	public double get_Variance( ) {
		return variance;
	}
	
	public double get_Standard_Deviation( ) {
		return std_dev;
	}
	
	public int get_Span( ) {
		return max - min;
	}
	
	public void set_String_Histogram() {
		idx_low = Parser.ASCII_LOW;
		idx_high = Parser.ASCII_HIGH;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(histogram);
		result = prime * result + idx_high;
		result = prime * result + idx_low;
		result = prime * result + max;
		long temp;
		temp = Double.doubleToLongBits(mean);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + min;
		temp = Double.doubleToLongBits(std_dev);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(sum);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(variance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Histogram)) {
			return false;
		}
		Histogram other = (Histogram) obj;
		if (!Arrays.equals(histogram, other.histogram)) {
			return false;
		}
		if (idx_high != other.idx_high) {
			return false;
		}
		if (idx_low != other.idx_low) {
			return false;
		}
		if (max != other.max) {
			return false;
		}
		if (Double.doubleToLongBits(mean) != Double
				.doubleToLongBits(other.mean)) {
			return false;
		}
		if (min != other.min) {
			return false;
		}
		if (Double.doubleToLongBits(std_dev) != Double
				.doubleToLongBits(other.std_dev)) {
			return false;
		}
		if (Double.doubleToLongBits(sum) != Double.doubleToLongBits(other.sum)) {
			return false;
		}
		if (Double.doubleToLongBits(variance) != Double
				.doubleToLongBits(other.variance)) {
			return false;
		}
		return true;
	}

}
