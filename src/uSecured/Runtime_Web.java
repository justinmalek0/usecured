package uSecured;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.swing.JOptionPane;

public abstract class Runtime_Web extends Analysis_Test {
	private String url;
	private Obj_Code inputTest;
	
    private List<String> List_Possibles;
    private List<Form> List_Forms;

	public Runtime_Web(	Controller c, Log l, String _url_) {
		super(c,l);
		set_Url( _url_ );
		setInputTest(null);
		setList_Possibles(new ArrayList<String>());
		setList_Forms(new ArrayList<Form>());
	}

	public abstract Runtime_Web newCopy( String url );
    protected abstract void extraHTMLParsing( BufferedReader reader, URL u );
    
    protected String parsePossible(String p) {
        // Intentionally just return what was passed in.
        // Child classes must override this function if they want extended functionality
        return p;
    }
    
    protected void addOtherPossibles() {
        // Intentionally do nothing.
        // Child classes must override this function if they want extended functionality
    }

	protected HttpURLConnection setupConnection( URL url ) throws IOException {
        // Child classes must override this function if they want extended functionality
        return (HttpURLConnection) url.openConnection();
    }

	@Override
	public Runtime_Web newCopy() {
		return newCopy(get_Url());
	}

	@Override
	public void run( ) {
		
		//msgLog("Runtime_Web Started");
		
		if ( !url.endsWith("/") ) {
			url += "/";
		}

        // check for other tests (currently runtime_form has another test) here and add it to the appropriate list
		addOtherPossibles();

        // Wait for inputTest(ObjCode) if applicable and add it to the appropriate list
        addPossiblesFromObjCodeTest();

		// Go through all possibilities from the GUI, from the Obj Code Test, from inputFromWebBypass [for Runtime_Form tests] 
		goThroughList( );
		
		finish();
	}

	private void addPossiblesFromObjCodeTest() {
		if ( getInputTest() != null ) {
			// Go through the possibilities from the object code scan if available
			// After it is complete though...
			// DESIGN: potential deadlock here :/ bc im only running one runtime test at a time
			waitForOtherMSThread( getInputTest() );

			for ( String hit : getInputTest().getListHits() ){
				Add_Possible( hit );
			}
		}
	}

	protected void Add_Possible(String hit) {
		Add_To_Possibles(hit);
	}

	protected void goThroughList( ) {
		for( String possible : List_Possibles ) {
			launchConnectWebThreadWithTimer( possible );
		}
	}

	protected void launchConnectWebThreadWithTimer(String url_postfix) {
		Thread t = new Thread(() -> connect_web( url_postfix ) );
		Timer tmr = new Timer();
		t.start();
		try {
			tmr.schedule( new InterruptTask(this), 6*1000 );
			t.join();
		} catch (InterruptedException e) {
			//msgLog("Response Timed Out - possible attack point?: "+url+url_postfix);
			Add_To_List(url+url_postfix+" timed out. May be vulnerable!");
			JOptionPane.showMessageDialog(null, "Make sure unit is still running\r\nClick OK when ready.", "Possible Unit Down?", JOptionPane.WARNING_MESSAGE);
		} finally {
			tmr.cancel();
			tmr.purge();
			tmr = null;
			t = null;
		}
	}

	private void connect_web( String possible) {
		URL u;
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		
		try {
			
			if( possible.startsWith("/")){
				possible = possible.substring(1);
			}
			
			u = new URL( url + possible );
			conn = setupConnection(u);
			
			// Set timeouts to *below* seconds.
			conn.setReadTimeout(2*1000);
			conn.setConnectTimeout(2*1000);
			//System.setProperty("java.net.HttpURLConnection.", "5000");
			//System.setProperty("java.net.HttpURLConnection.", "5000");
			
			conn.connect();
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			// calling read so hopefully the timeout will happen...
			reader.mark(10);
			reader.read();
			reader.reset();

			//msgLog("Page ("+ u.toExternalForm()+") Response: "+ conn.getResponseMessage());
			
			// DESIGN/NOTES: potential fix? only get the response code...(before opening reader)
			// see evernote: Java: How to send HTTP request GET/POST in Java
			
			if ( checkResponse( u, conn ) ) {
				extraHTMLParsing( reader, u );
			}
			
		} catch (MalformedURLException e) {
			msgLog("Bad URL: "+url+possible);
			e.printStackTrace();
		} catch (FileNotFoundException fe) {
			handleError( fe, url, possible );
			//fe.printStackTrace();
		} catch ( SocketTimeoutException se ) {
			//msgLog("Response Timed Out---possible attack point?: "+url+possible);
			// QUESTION: Add this to the list also? ANSWER: i do not think so.
			se.printStackTrace();
		} catch (IOException e) {
			handleError( e, url, possible);
			//e.printStackTrace();
		} finally {
			try {
				if ( reader != null ) {
					reader.close();
					reader = null;
				}
				if ( conn != null ) {
					conn.disconnect();
					conn = null;
				}
			} catch (IOException e) {
				// intentionally ignore this
			}
		}
	}

	protected void handleError(IOException e, String u, String p) {
		//msgLog("No form items specified: "+u+p);
	}

	protected void handleError(FileNotFoundException fe, String u,
			String p) {
		//msgLog("Page not Available: "+u+p);
	}

	protected boolean checkResponse( URL u, HttpURLConnection conn ) throws IOException {
		if( conn.getResponseCode() / 100  == 2 ) {
			Add_To_List(u.toExternalForm());
			return true;
		}
		return false;
	}

	public String get_Url() {
		return url;
	}

	public void set_Url(String url) {
		this.url = url;
	}

	/**
	 * @return the list_Possibles
	 */
	public List<String> getList_Possibles() {
		return List_Possibles;
	}

	/**
	 * @param list_Possibles the list_Possibles to set
	 */
	public void setList_Possibles(List<String> list_Possibles) {
		List_Possibles = list_Possibles;
	}

	@Override
	public String toString() {
		if ( url == null )
		{
			return "Web Runtime";
		}
		
		if(isComplete()) {
			return "Completed: " + this.url + ": Web Runtime";
		}
		
		return this.url + ": Web Runtime";
	}

	@Override
	public void finish() {
		//msgLog("Runtime_Web Ended");
		super.finish();
	}

	public Obj_Code getInputTest() {
		return inputTest;
	}

	protected void setInputTest(Obj_Code inputTest) {
		this.inputTest = inputTest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Runtime_Web)) {
			return false;
		}
		Runtime_Web other = (Runtime_Web) obj;
		if (url == null) {
			if (other.url != null) {
				return false;
			}
		} else if (!url.equals(other.url)) {
			return false;
		}
		if ( inputTest == null && other.getInputTest() != null ) {
			return false;
		} else if ( inputTest != null && other.getInputTest() == null ) {
			return false;
		} else if ( inputTest != null && !inputTest.equals(other.getInputTest()) ) {
			return false;
		}
		return true;
	}
    
    public boolean Add_To_Possibles( String possibility )
    {
        if ( possibility.length() == 0 ) {
            return false;
        }
        
        for( String p : List_Possibles )
        {
            if ( p.equalsIgnoreCase( possibility ) )
            {
                return false;
            }
        }
        
        List_Possibles.add( possibility );
        
        return true;
    }
    
    public boolean Add_To_Possibles( ArrayList<String> List )
    {
        for( String p : List )
        {
        	Add_To_Possibles( p );
        }
        
        return true;
    }
    
    public boolean Add_To_Possibles( String[] strings )
    {
        for( String p : strings )
        {
        	Add_To_Possibles( p );
        }
        
        return true;
    }
    
    // NOTES: ADD PRECEDENCE VARIABLE - which Form/FormItem should be stored?
    // currently the already stored information has precedence
    // Sort of workaround, GUI first, Web Bypass next, then (as a last resort since
    // it probably will not be that great) the Obj Code Output
    public boolean Add_To_Forms( Form possibility )
    {
        for( Form f : List_Forms )
        {
            if ( f.equals( possibility ) )
            {
                if (f.Name() == null) {
                    f.Name( possibility.Name() );
                }
                if (f.Encode() == null) {
                    f.Encode( possibility.Encode() );
                }
                if (f.FromURL() == null) {
                    f.FromURL( possibility.FromURL() );
                }
                if (f.Credential() == null) {
                    f.Credential( possibility.Credential() );
                }
            	for(FormItem fip : possibility.getFormItems()){
            		boolean newValue = true;
            		for(FormItem fi : f.getFormItems()){
            			if ( fip.equals(fi) ) {
                            if (fi.Value().length() == 0 || fi.Value().equalsIgnoreCase("novalue") ) {
                                fi.Value( fip.Value() );
                            }
                            if (fi.Type().length() == 0 ) {
                                fi.Type( fip.Type() );
                            }
            				newValue = false;
            				break;
            			}
            		}
            		if(newValue){
            			f.Add_Form_Item(fip);
            		}
            	}
                return false;
            }
        }
        
        List_Forms.add( possibility );
        
        return true;
    }
    
    public boolean Add_To_Forms( ArrayList<Form> List )
    {
        for( Form f : List )
        {
        	Add_To_Forms( f );
        }
        
        return true;
    }
    
    public boolean Add_To_Forms( Form[] forms )
    {
        for( Form f : forms )
        {
        	Add_To_Forms( f );
        }
        
        return true;
    }

	public List<Form> getList_Forms() {
		return List_Forms;
	}

	public void setList_Forms(List<Form> list_Forms) {
		List_Forms = list_Forms;
	}
	
	public void convertFormsToPossibles( ){
		for( Form f : List_Forms ){
			List_Possibles.add(f.PrintAll());
		}
		// List_Forms.clear();
	}

    public static String readWebpage( BufferedReader reader ) {
        StringBuilder pageBuild = new StringBuilder();
        
        try {
            String line;
            while ( ( line = reader.readLine() ) != null ) {
                //msgLog(line);
                pageBuild.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pageBuild.toString();
    }
}
