package uSecured;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

public abstract class Analysis_Test extends MicroSecured_Thread {
	
	public static final String THREAD_NAME = "Analysis Test";
	
    private List<String> List_Hits;

    public Analysis_Test( Controller c, Log l ) {
        setMsgQ( new ArrayBlockingQueue<Message>( 500 ) );
        setLog( l );
        setController( c );
        List_Hits = new ArrayList<String>();
    }
    
    @Override
    public abstract void run();
    @Override
	public abstract String toString();
    @Override
	public abstract Analysis_Test newCopy();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isRunning() ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Analysis_Test)) {
			return false;
		}
		Analysis_Test other = (Analysis_Test) obj;
		if (isRunning() != other.isRunning()) {
			return false;
		}
		return true;
	}
    
    public boolean Add_To_List( String possibility )
    {
        for( String Hit : List_Hits )
        {
            if ( Hit.equalsIgnoreCase( possibility ) )
            {
                return false;
            }
        }
    	
        if ( possibility.length() == 0 ) {
        	return false;
        }
        
    	List_Hits.add( possibility );
        
        return true;
    }
    
    public boolean Add_To_List( ArrayList<String> List )
    {
        for( String Hit : List )
        {
            Add_To_List( Hit );
        }
        
        return true;
    }
    
    public boolean Add_To_List( String[] strings )
    {
        for( String Hit : strings )
        {
            Add_To_List( Hit );
        }
        
        return true;
    }
    
    public String Hits_To_String( ) {
        String result = "";
        
        if(List_Hits.isEmpty()){
        	return "No issues found."+ System.lineSeparator();
        }
        
        for ( String Hit : List_Hits )
        {
            result = result + "Result: " + Hit + System.lineSeparator();
        }
        
        // logging.message(new Message("HITS TO STRING: "+result));
        
        return result;
    }
	
	@Override
	public String get_Thread_Name( ) {
		return Analysis_Test.THREAD_NAME;
	}
	
	public List<String> getListHits( ) {
		return List_Hits;
	}
}
