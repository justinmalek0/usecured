package uSecured;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Source_Input extends MicroSecured_Thread {
	
	public static final String THREAD_NAME = "Source Scan Input";
	
	private File file;
	private List<String> paths;
	
	private List<Source_File> parsedFiles;

	public Source_Input(Controller c, Log l, File f, String p) {
		setController(c);
		setLog(l);
		setFile(f);
		paths = new ArrayList<String>();
		if(p != null) {
			addPath(p);
		}
		Files(new ArrayList<Source_File>());
	}
	
	public Source_Input(Controller c, Log l, File f, List<String> ps) {
		setController(c);
		setLog(l);
		setFile(f);
		paths = ps;
	}

	public File getFile() {
		return file;
	}

	public Source_Input setFile(File file) {
		this.file = file;
		return this;
	}

	public List<String> getPaths() {
		return paths;
	}

	public Source_Input setPaths(List<String> paths) {
		this.paths = paths;
		return this;
	}
	
	public void addPath(String path) {
		String[] ps = path.split("(\r|\n)+");
		for( String p : ps ) {
			paths.add(p);
		}
	}
	
	public void addPath(List<String> paths) {
		for( String p : paths ){
			addPath(p);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((paths == null) ? 0 : paths.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Source_Input other = (Source_Input) obj;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		if (paths == null) {
			if (other.paths != null)
				return false;
		} else if (!paths.equals(other.paths))
			return false;
		return true;
	}

	@Override
	public void run() {
		// TODO: Open (each) .c/.h file
		// TODO: Read the file
		// NOTES: parse by ; " ( ) { } , #
		// TODO: Create a File class with:
		//			Includes (only ones with '"' - search folder for them)
		//			Variable (type: global, local, Name)
		//			Function (args: input Variables, Local_Vars: type, Name)
		//			Printf_Scan (new class): look for sprintf, snprintf, printf...
		// 			...
		// TODO: send data its message Q (which is the rx queue of another parser thread
		// 			[like for bin_input]) which will then send it to all the appropo Source Scan
		//			tests
		while ( isRunning() ) {
			if ( file != null ) {
				parseFile(file);
			}
			
			for( String path : paths ) {
				File temp = new File(path);
				if(temp.isDirectory()) {
					for( File f : temp.listFiles()){
						if(f.isFile()){
							parseFile(f);
						}
					}
				} else if(temp.isFile()) {
					parseFile(temp);
				}
			}
			
			// Send Files to Parser/Source_Scan (sub-)class
			try {
				getMsgQ().put(new Parsed_File_Message(get_Thread_Name()+" Files", Files()));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			finish();
		}
	}

	private void parseFile(File f) {
		String extension = f.getName().substring(f.getName().lastIndexOf('.')+1, f.getName().length());
		if( //extension.equalsIgnoreCase("cpp")	||
			extension.equalsIgnoreCase("c")		||
			extension.equalsIgnoreCase("h")			) {
			Source_File parsedFile = new Source_File(f.getName());
			parsedFile.Lines( readFile(f) );
			// msgLog(parsedFile.Lines());
			Files().add( parsedFile );
		}
	}

	private String readFile(File f) {
		StringBuilder builder = new StringBuilder();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(f));
			String line = in.readLine();
			while(line != null) {
				builder.append(line+System.lineSeparator());
				line = in.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if ( in != null ) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return builder.toString();
	}

	@Override
	public String get_Thread_Name() {
		return Source_Input.THREAD_NAME;
	}

    @Override
    public void finish() {
        try {
            getMsgQ().put(new Stop_Message(get_Thread_Name()+" Source Input is Complete"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //msgLog(new Message(get_Thread_Name()+" Ended"));
        super.finish();
    }

	@Override
	public String toString() {
        if ( file == null ) {
        	if( paths.isEmpty() ){
        		return get_Thread_Name();
        	} else if (paths.size() == 1) {
        		return paths.get(0)+" "+get_Thread_Name();
        	}
    		return paths.get(0)+"+ "+get_Thread_Name();
        }
        return file.getName()+" "+get_Thread_Name();
	}

	@Override
	public Source_Input newCopy() {
		return new Source_Input(getController(), getLog(), file, paths);
	}

	/**
	 * @return the parsedFiles
	 */
	public List<Source_File> Files() {
		return parsedFiles;
	}

	/**
	 * @param parsedFiles the parsedFiles to set
	 */
	private void Files(List<Source_File> parsedFiles) {
		this.parsedFiles = parsedFiles;
	}

}
