package uSecured;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public abstract class Runtime_Form extends Runtime_Web {
	
	private Form Obj_Code_Form_Current;
	private Web_Bypass inputFromWebBypass;
	private boolean b_Add_Authorization;

	public Runtime_Form(Controller c, Log l, String _url_) {
		super(c, l, _url_);
		inputFromWebBypass = null;
		Obj_Code_Form_Current = null;
		b_Add_Authorization = false;
	}

	@Override
	public abstract Runtime_Form newCopy(String url);
	
	@Override
	public boolean equals(Object obj){
		return super.equals(obj);
	}

	@Override
	protected HttpURLConnection setupConnection(URL url) throws IOException {
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		
		if ( Obj_Code_Form_Current.getFormItems().isEmpty() ) {
			throw new IOException("No form items specified for this form!");
		}
		
		StringBuilder query = new StringBuilder();
		query.append(printFormItem(Obj_Code_Form_Current.getFormItems().get(0), false));
		for(int i = 1 ; i < Obj_Code_Form_Current.getFormItems().size() ; i++ ) {
			query.append(printFormItem(Obj_Code_Form_Current.getFormItems().get(i), true));
		}
		
		switch( Obj_Code_Form_Current.Method() ){
		case "get" :
			httpConn = (HttpURLConnection)( new URL( url.toExternalForm()+"?"+query.toString() ).openConnection() );
			httpConn.setRequestMethod(Obj_Code_Form_Current.Method().toUpperCase());
			break;
		case "post" :
			httpConn.setRequestMethod(Obj_Code_Form_Current.Method().toUpperCase());
			httpConn.setDoOutput(true);
			if( Obj_Code_Form_Current.Encode() != null ) {
				httpConn.setRequestProperty( "Content-Type", Obj_Code_Form_Current.Encode());
			}
			httpConn.setRequestProperty( "charset", StandardCharsets.UTF_8.name());
			httpConn.setRequestProperty("User-Agent", "Mozilla/5.0");
			httpConn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			// httpConn.setRequestProperty( "Content-Length", Integer.toString( query.toString().getBytes(StandardCharsets.UTF_8).length ));
			// httpConn.setRequestProperty("Connection", "close");
			// System.setProperty("http.keepAlive", "false");
			httpConn.setUseCaches(false);
			
			try ( OutputStream out = httpConn.getOutputStream() ) {
				out.write(query.toString().getBytes(StandardCharsets.UTF_8));
			}
			break;
		}

		if ( b_Add_Authorization && Obj_Code_Form_Current.Credential() != null ) {
			httpConn.setRequestProperty("Authorization", "Basic " + Obj_Code_Form_Current.Credential().toBase64WebRequest());
		}
		
		return httpConn;
	}

	@Override
	protected void Add_Possible(String hit) {
		Add_To_Forms(Form.FormFromPrintAll(hit));
	}

	@Override
	protected void goThroughList() {
		for( Form form : getList_Forms() ) {
			setObj_Code_Form_Current( form );
			launchConnectWebThreadWithTimer( form.Action() );
		}
	}

	protected String printFormItem(FormItem formItem, boolean printAmpersand) {
		return formItem.toFormSubmitString(printAmpersand);
	}

	@Override
	protected void extraHTMLParsing(BufferedReader reader, URL url) {
		// Intentionally do nothing.
        // Child classes must override this function if they want extended functionality
	}

	@Override
	protected void addOtherPossibles() {
		if( getInputFromWebBypass() != null ) {
			// Go through the possibilities from the object code scan if available
			// After it is complete though...

			waitForOtherMSThread( getInputFromWebBypass() );
			
			for(Form f : getInputFromWebBypass().getList_Forms() ) {
				Add_To_Forms(f);
			}
		}
	}

	@Override
	public String toString() {
		return super.toString() + " Form Submission";
	}

	public Web_Bypass getInputFromWebBypass() {
		return inputFromWebBypass;
	}

	public void setInputFromWebBypass(Web_Bypass inputFromWebBypass) {
		this.inputFromWebBypass = inputFromWebBypass;
	}

	@Override
	public Post_Scan getInputTest() {
		return (Post_Scan) super.getInputTest();
	}
	
	public void setInputTest( Post_Scan inputTest ) {
		super.setInputTest(inputTest);
	}

	public void setObj_Code_Form_Current(Form obj_Code_Form_Current) {
		Obj_Code_Form_Current = obj_Code_Form_Current;
	}

	public Form getObj_Code_Form_Current() {
		return Obj_Code_Form_Current;
	}

	public boolean Add_Authorization() {
		return b_Add_Authorization;
	}

	public Runtime_Form Add_Authorization(boolean b) {
		this.b_Add_Authorization = b;
		return this;
	}

}
