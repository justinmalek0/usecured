package uSecured;

import java.util.Base64;
import java.util.concurrent.BlockingQueue;

public class Base_64 extends Decoder {
	
	public static final int MIN_DECODE_SIZE = 4;
	public static final boolean ON_THE_FLY = true;

	public Base_64(int q_in_size, BlockingQueue<Message> q_out, Controller c,
			Log l) {
		super(q_in_size, q_out, c, l);
	}

	@Override
	public byte[] Decode(byte[] in_bytes) throws IllegalArgumentException {
		// super.get_Log().message(new Message("Base_64 input: "+in_bytes.toString()));
    	// for( byte b : in_bytes) {
    	// 	super.get_Log().message(new Message("Base_64 input: " + b));	
    	// }
		return Base64.getDecoder().decode(in_bytes);
	}

	@Override
	public byte min_size_needed() {
		return MIN_DECODE_SIZE;
	}

	@Override
	public boolean b_On_The_Fly_Possible() {
		return ON_THE_FLY;
	}

	@Override
	public String toString() {
		return "Base64 Encoding";
	}

	@Override
	public Base_64 newCopy() {
		return new Base_64(100, get_Q_Out(), getController(), getLog());
	}

}
