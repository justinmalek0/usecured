package uSecured;

import org.jsoup.nodes.Attributes;
import org.jsoup.parser.Tag;

public class FormInput extends FormItemElement {
	
	private FormInputType type = FormInputType.Not_Set;

	public FormInput(Tag tag, String baseUri) {
		super(tag, baseUri);
	}

	public FormInput(Tag tag, String baseUri, Attributes attributes) {
		super(tag, baseUri, attributes);
	}

	public String inputType() {
		if( type == FormInputType.Not_Set ) {
			switch(super.attr("type").toLowerCase()){
			case "submit" :
				inputType(FormInputType.Submit);
				break;
			case "reset" :
				inputType(FormInputType.Reset);
				break;
			case "text" :
				inputType(FormInputType.Text);
				break;
			case "password" :
				inputType(FormInputType.Password);
				break;
			case "radio" :
				inputType(FormInputType.Radio);
				break;
			case "checkbox" :
				inputType(FormInputType.Checkbox);
				break;
			case "button" :
				inputType(FormInputType.Button);
				break;
			default :
				inputType(FormInputType.Unhandled_Type);
				break;
			}
		}
		if ( type == FormInputType.Unhandled_Type ) {
			return super.tagName();
		}
		return type.toString();
	}

	public void inputType(FormInputType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return inputType()+" "+super.toString();
	}

}
