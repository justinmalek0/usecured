package uSecured;

import java.util.ArrayList;
import java.util.List;

public class Source_Function {

	private String name;
	private String namedRetType;
	private Variable_Type retType;
	private List<Source_Variable> args;
	private List<Source_Variable> localVars;
	private String functionBody;

	public Source_Function(String nrt, String n) {
		namedRetType = nrt;
		name = n;
		args = new ArrayList<Source_Variable>();
		localVars = new ArrayList<Source_Variable>();
	}

	public String getName() {
		if(name==null){
			return "";
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamedRetType() {
		if(namedRetType==null){
			return "";
		}
		return namedRetType;
	}

	public void setNamedRetType(String namedRetType) {
		this.namedRetType = namedRetType;
	}

	public Variable_Type getRetType() {
		return retType;
	}

	public void setRetType(Variable_Type retType) {
		this.retType = retType;
	}

	public List<Source_Variable> getArgs() {
		return args;
	}

	public void setArgs(List<Source_Variable> args) {
		this.args = args;
	}

	public List<Source_Variable> getLocalVars() {
		return localVars;
	}

	public void setLocalVars(List<Source_Variable> localVars) {
		this.localVars = localVars;
	}

	public String FunctionBody() {
		if(functionBody==null){
			return "";
		}
		return functionBody;
	}

	public void FunctionBody(String functionBody) {
		this.functionBody = functionBody;
	}

}
