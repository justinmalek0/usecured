package uSecured;

import java.util.Base64;

public class Credential {

	private String username;
	private String password;
	
	public Credential(String u, String p) {
		setUsername(u);
		setPassword(p);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "usr: "+username+" | pwd: " + password;
	}

	public String toBase64WebRequest() {
		String usr_pwd = username+":"+password;
		return Base64.getEncoder().encodeToString(usr_pwd.getBytes());
	}

}
