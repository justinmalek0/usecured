package uSecured;

import java.io.BufferedReader;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Web_Bypass extends Runtime_Web {

	public Web_Bypass(Controller c, Log l, String _url_) {
		super(c, l, _url_);
	}

	@Override
	public Web_Bypass newCopy(String url) {
		return new Web_Bypass(getController(), getLog(), url);
	}

	@Override
	public String toString() {
		return super.toString() + " Webpage Authentication Bypass";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Web_Bypass)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

	@Override
	public Webpage_Scan getInputTest() {
		return (Webpage_Scan) super.getInputTest();
	}
	
	public void setInputTest( Webpage_Scan inputTest ) {
		super.setInputTest(inputTest);
	}

	@Override
	protected void extraHTMLParsing(BufferedReader reader, URL url) {
		
		String webpage = Runtime_Web.readWebpage( reader );
		
		Document page = Jsoup.parse(webpage, url.toExternalForm());
		
		Elements forms = page.body().select("form");
		
		for (Element form : forms){
			Form frm = Form.createForm( form );
			frm.FromURL( url.toExternalForm() );
			Add_To_Forms(frm);
		}
	}

}
