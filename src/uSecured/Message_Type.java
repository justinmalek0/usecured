package uSecured;

public enum Message_Type {
	Test,
	Obj_File,
	Start_Tests,
	_Stop_Message,
    String_Message,
    Bytes_Message,
    Thread_Complete,
    Source_File,
    Parsed_File,
}
