package uSecured;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Post_Scan extends Obj_Code {

	public Post_Scan(Controller c, Log l, File f) {
		super(c, l, f);
	}

	@Override
	public Post_Scan newCopy(File newFile) {
		return new Post_Scan(getController(), getLog(), newFile);
	}

	@Override
	public boolean b_Check_String(String msg) {
		
		String line_low = msg.trim().toLowerCase();
		
		if( line_low.indexOf("form") >=0 ) {
			// msgLog("POST_SCAN Form: "+msg);
			Document page = Jsoup.parse(msg);
			Elements forms = page.select("form");
			for (Element form : forms){
				Form frm = Form.createForm( form );
				Add_To_List(frm.PrintAll());
			}
		}
		
		// Always returning false because I am adding the hits piecewise above
		return false;
	}

	@Override
	public String toString() {
		return super.toString() + " Post Fcn Scan";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Post_Scan)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

}
