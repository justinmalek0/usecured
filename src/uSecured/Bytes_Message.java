package uSecured;

public class Bytes_Message extends Message {
	
	private final byte[] data;

	public Bytes_Message( String m, byte[] d ) {
		super(Message_Type.Bytes_Message, m);
		data = d;
	}

	public byte[] getData() {
		return data;
	}

}
