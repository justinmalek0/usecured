package uSecured;

import java.io.File;

public class Webpage_Scan extends Obj_Code {

	public Webpage_Scan(Controller c, Log l, File f) {
		super(c, l, f);
	}

	@Override
	public Webpage_Scan newCopy(File newFile) {
		return new Webpage_Scan(getController(), getLog(), newFile);
	}

	@Override
	public boolean b_Check_String(String msg) {
		
		String[] possible_pages = msg.trim().split("(\\s)+");
		
		for( String possible : possible_pages ) {
			if( ( possible.length() > 0                           ) &&
				( ( possible.toLowerCase().contains(".htm")  ) ||
				  ( possible.toLowerCase().contains(".shtm") )    )    ) {
				Add_To_List(possible);
			}
		}
		
		// Always returning false because I am adding the hits piecewise above
		return false;
	}

	@Override
	public String toString() {
		return super.toString() + " Webpage Scan";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Webpage_Scan)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

}
