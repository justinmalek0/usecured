package uSecured;

import java.util.ArrayList;
import java.util.List;

public class Source_File {
	
	private String filename;
	private List<String> listPounds;
	private List<Source_Variable> listFileVars;
	private List<Source_Function> listFunctions;
	private String lines;

	public Source_File(String name) {
		Filename(name);
		Variables(new ArrayList<Source_Variable>());
		Functions(new ArrayList<Source_Function>());
		Pounds(new ArrayList<String>());
	}

	/**
	 * @return the filename
	 */
	public String Filename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public Source_File Filename(String filename) {
		this.filename = filename;
		return this;
	}

	/**
	 * @return the listFileVars
	 */
	public List<Source_Variable> Variables() {
		return listFileVars;
	}

	/**
	 * @param listFileVars the listFileVars to set
	 */
	public Source_File Variables(List<Source_Variable> listFileVars) {
		this.listFileVars = listFileVars;
		return this;
	}

	/**
	 * @return the listFunctions
	 */
	public List<Source_Function> Functions() {
		return listFunctions;
	}

	/**
	 * @param listFunctions the listFunctions to set
	 */
	public Source_File Functions(List<Source_Function> listFunctions) {
		this.listFunctions = listFunctions;
		return this;
	}

	/**
	 * @return the lines
	 */
	public String Lines() {
		return lines;
	}

	/**
	 * @param lines the lines to set
	 */
	public void Lines(String lines) {
		this.lines = lines;
		//parseLines();
	}

	@SuppressWarnings("unused")
	private void parseLines() {
		listFileVars.clear();
		listFunctions.clear();

		StringBuilder builderPound = new StringBuilder();
		StringBuilder builderVar = new StringBuilder();
		StringBuilder builderBrace = new StringBuilder();
		String[] lines = Lines().split("(\r|\n)+");
		boolean openStreamComment = false;
		boolean continuePound = false;
		int countBraceOpen = 0;
		int countBraceClose = 0;
		
		for(int i = 0 ; i < lines.length ; i++){
			if(!lines[i].isEmpty()){
				String tempLine = lines[i].trim();
				
				// Check for comments
				int streamCommentEnd = tempLine.indexOf("*/");
				
				if( openStreamComment && streamCommentEnd > -1 ){
					tempLine = tempLine.substring(streamCommentEnd+"*/".length());
					openStreamComment = false;
				}

				if( !openStreamComment ) {
					int lineComment = tempLine.indexOf("//");
					int streamCommentStart = tempLine.indexOf("/*");
					if ( lineComment > -1 && streamCommentStart > -1 ) {
						if ( lineComment > streamCommentStart){
							lineComment = streamCommentStart;
							openStreamComment = true;
						}
					}
					
					if( lineComment > 0 ) {
						tempLine = tempLine.substring(0, lineComment);
					}
					
					// only care if the line does not start with a #
					if( tempLine.startsWith("#") || continuePound ){
						builderPound.append(tempLine);
						continuePound = tempLine.endsWith("\\");
						if(!continuePound){
							Pounds().add(builderPound.toString());
							builderPound.setLength(0);
						}
					} else {
						boolean keepParsing = true;
						while(keepParsing){
							int semicolonIdx = tempLine.indexOf(";");
							int openBraceIdx = tempLine.indexOf("{");
							int closeBraceIdx = tempLine.indexOf("}");
							if( openBraceIdx == -1 && semicolonIdx == -1 ) {
								builderVar.append(tempLine);
								keepParsing = false;
							} else if( openBraceIdx == -1 && semicolonIdx != -1 ) {
								builderVar.append(tempLine.substring(0,	semicolonIdx));
								Variables().add(new Source_Variable(Scope.GLOBAL, builderVar.toString(), ""));
								builderVar.setLength(0);
							} else if( openBraceIdx != -1 && semicolonIdx == -1 ) {
								builderBrace.append(builderVar.toString()+tempLine.substring(0,	openBraceIdx));
								countBraceOpen++;
							}
							// TODO: lots to do here still...
						}
					}
				}
			}
		}
	}

	/**
	 * @return the listPounds
	 */
	public List<String> Pounds() {
		return listPounds;
	}

	/**
	 * @param listPounds the listPounds to set
	 */
	public void Pounds(List<String> listPounds) {
		this.listPounds = listPounds;
	}

}
