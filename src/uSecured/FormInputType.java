package uSecured;

public enum FormInputType {
	Not_Set,
	Submit,
	Reset,
	Text,
	Password,
	Radio,
	Checkbox,
	Button,
	Unhandled_Type
}
