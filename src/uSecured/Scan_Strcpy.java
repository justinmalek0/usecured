package uSecured;

public class Scan_Strcpy extends Source_Scan {

	public Scan_Strcpy(Controller c, Log l, Source_Input si) {
		super(c, l, si);
	}

	@Override
	public String SearchString() {
		return "strcpy";
	}

	@Override
	protected void check(Source_File file, String function, String[] args) {
		if(function.equals(SearchString())){
			Add_To_List(file.Filename()+": Consider using strncpy in place of "+SearchString()+" or "+args[0]+" could overflow.");
		}
	}

	@Override
	public Scan_Strcpy newCopy(Source_Input si) {
		return new Scan_Strcpy(getController(), getLog(), si);
	}

	@Override
	public String toString() {
		return super.toString() + " String Copy";
	}

}
