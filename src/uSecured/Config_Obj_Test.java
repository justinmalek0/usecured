package uSecured;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

public class Config_Obj_Test extends JFrame {

    private static final long serialVersionUID = -2599335646376558538L;

    private Controller ctlr;
    private Log logging;

    private JPanel contentPane;
    private JTextField txt_Binary_File_Path;
    private JList<Obj_Code> listObjTests;
    private JComboBox<Decoder> comboBox_Decode;
    private JButton btnSave;
    private JButton btnExit;
    private JLabel lblTestList;
    private JLabel lblEncoding;
    private JLabel lblPathToBinary;
    private final JFileChooser fc;
    private File selected_file;

    private List<Obj_Code> List_Active_Tests;
    private JLabel lblCompression;
    private JComboBox<Bin_Input> comboBox_Decompress;
    
    /**
     * Create the frame.
     */
    public Config_Obj_Test( Controller c,Log l ) {
        ctlr = c;
        logging = l;
        
        List_Active_Tests = new ArrayList<Obj_Code>();
        
        fc = new JFileChooser();
        selected_file = null;

        setResizable(false);
        setTitle("Object Code Tests");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 401, 319);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        DefaultListModel<Obj_Code> listmodel = new DefaultListModel<Obj_Code>();
        listmodel.addElement(new Key_Scan( ctlr, logging, null) );
        listmodel.addElement(new Post_Scan( ctlr, logging, null) );
        listmodel.addElement(new Webpage_Scan( ctlr, logging, null) );
        listObjTests = new JList<Obj_Code>(listmodel);
        listObjTests.setToolTipText("Select multiple by using Control+Click");
        listObjTests.setBounds(10, 30, 184, 250);
        contentPane.add(listObjTests);
        
        comboBox_Decode = new JComboBox<Decoder>();
        comboBox_Decode.addItem(new No_Decoder(1, null, ctlr, logging));
        comboBox_Decode.addItem(new Base_64(1, null, ctlr, logging));
        comboBox_Decode.setSelectedIndex(0);
        comboBox_Decode.setBounds(204, 28, 184, 20);
        contentPane.add(comboBox_Decode);
        
        btnSave = new JButton("Save");
        btnSave.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
    			boolean error = false;
    			List_Active_Tests.clear();
    			
    			if ( ( selected_file == null                                                    ) ||
    				 ( !txt_Binary_File_Path.getText().equals(selected_file.getAbsolutePath() ) )    )
    			{
    				selected_file = new File( txt_Binary_File_Path.getText() );
    			}
    			if ( !selected_file.exists() )
    			{
    				logging.message( new Message( "Please select an existing file.\r\n" + selected_file.getAbsolutePath() + " does not exist!" ) );
    				error = true;
    			}
    			if ( !error && !selected_file.canRead() )
    			{
    				logging.message( new Message( selected_file.getAbsolutePath() + " must be readable!" ) );
    				error = true;
    			}

    			if ( !error )
    			{
    				// Pass correct Bin/Zip_Input type back to Controller correctly...
    				ctlr.message(new File_Message("Object File",
    						selected_file,
    						((Decoder)comboBox_Decode.getSelectedItem()).newCopy(),
    						((Bin_Input)comboBox_Decompress.getSelectedItem()).newCopy(selected_file) ) );
    				
        			List_Active_Tests = listObjTests.getSelectedValuesList();
        			
        			if ( !List_Active_Tests.isEmpty() )
        			{
	        			for( Obj_Code test : List_Active_Tests )
	        			{
		        			//logging.message( new Message( "Want to run: " + test ) );
		        			ctlr.message(new Obj_Tests_Message("Obj Code Tests", test.newCopy(selected_file)));
		        		}
        			}
        			else
        			{
        				logging.message( new Message( "No Object Code Tests Selected" ) );
        			}
    			}
        	}
        });
        btnSave.setBounds(204, 257, 89, 23);
        contentPane.add(btnSave);
        
        btnExit = new JButton("Exit");
        btnExit.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		dispose();
        	}
        });
        btnExit.setBounds(292, 257, 89, 23);
        contentPane.add(btnExit);
        
        lblTestList = new JLabel("Test List");
        lblTestList.setBounds(10, 11, 58, 23);
        contentPane.add(lblTestList);
        
        lblEncoding = new JLabel("Encoding");
        lblEncoding.setBounds(204, 12, 177, 20);
        contentPane.add(lblEncoding);
        
        lblPathToBinary = new JLabel("Path To Binary File");
        lblPathToBinary.setBounds(204, 154, 119, 20);
        contentPane.add(lblPathToBinary);
        
        txt_Binary_File_Path = new JTextField();
        txt_Binary_File_Path.setBounds(204, 173, 184, 20);
        contentPane.add(txt_Binary_File_Path);
        txt_Binary_File_Path.setColumns(10);
        
        JButton btnBrowse = new JButton("Browse...");
        btnBrowse.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if ( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION )
        		{
        			selected_file = fc.getSelectedFile();
        			txt_Binary_File_Path.setText( selected_file.getAbsolutePath() );
        		}
        	}
        });
        btnBrowse.setBounds(299, 194, 89, 23);
        contentPane.add(btnBrowse);
        
        lblCompression = new JLabel("Compression");
        lblCompression.setBounds(204, 59, 177, 20);
        contentPane.add(lblCompression);
        
        comboBox_Decompress = new JComboBox<Bin_Input>();
        comboBox_Decompress.addItem(new Bin_Input(null, ctlr, logging));
        comboBox_Decompress.addItem(new Zip_Input(null, ctlr, logging));
        comboBox_Decompress.addItem(new GZip_Input(null, ctlr, logging));
        comboBox_Decompress.setSelectedIndex(0);
        comboBox_Decompress.setBounds(204, 75, 184, 20);
        contentPane.add(comboBox_Decompress);
        
        debug_setup();
    }

	private void debug_setup() {
        txt_Binary_File_Path.setText("C:\\Users\\justinmalek0\\Documents\\iBTX_22.bin");
	}
}
