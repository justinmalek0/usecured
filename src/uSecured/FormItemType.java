package uSecured;

public enum FormItemType {
	Not_Set,
	Input,
	Select,
	TextArea,
	Unhandled_Type
}
