package uSecured;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Arrays;

public class Form_Overflow extends Runtime_Form {
	
	private String overflowValue = null;

	public Form_Overflow(Controller c, Log l, String _url_) {
		super(c, l, _url_);
		char[] ov = new char[ URL_Overflow.OVERFLOW_INCREMENT ];
		Arrays.fill(ov, 'b');
		overflowValue = new String(ov);
		Add_Authorization(true);
	}

	@Override
	public Form_Overflow newCopy(String url) {
		return new Form_Overflow(getController(), getLog(), url);
	}

	@Override
	public String toString() {
		return super.toString() + " Overflow";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Form_Overflow)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

	public String getOverflowValue() {
		return overflowValue;
	}

	public void setOverflowValue(String overflowValue) {
		this.overflowValue = overflowValue;
	}

	@Override
	protected String printFormItem(FormItem formItem, boolean printAmpersand) {
		if(formItem.Type().equals("submit")){
			return formItem.toFormSubmitString(printAmpersand);			
		}
		return formItem.toFormSubmitString(printAmpersand, overflowValue);
	}
	
	// NOTES/QUESTION: need to overflow the items in the form. maybe have to do it in setupConnection
	// as part of building the StringBuilder query - add a new function Form_Overflow will override that will
	// print the overflow value. - do i need multiple overflow values like in URL_Overflow? or
	// just one really big one? aka close to 8K? - right now just 1K
	// need to add input type to form items (and parsing for form items)? to try only OV text inputs...
	// as well as try singular submits...
	
	@Override
	protected boolean checkResponse(URL u, HttpURLConnection conn)
			throws IOException {
		int response = conn.getResponseCode();
		
		if( response == -1 ) {
			Add_To_List(u.toExternalForm() + " got bad response! Potential overflow with form " + u.getPath());
			return false;
		}
		
		return true;
	}

	// NOTES: reload the main page after sending the bloated form...to check to make sure the server is still alive!
	// QUESTION: Do i need to Add_To_List here? maybe set the timeouts longer and only let the Add_to_List in Runtime_Web:launchConnectWebThreadWithTimer add it...
	@Override
	protected void extraHTMLParsing(BufferedReader reader, URL u) {
		// reload the main page to see if the unit is still responding
		URL baseURL = null;
		HttpURLConnection conn = null;
		try {
			baseURL = new URL( u.getProtocol() + "://" + u.getHost() );
			conn = (HttpURLConnection) baseURL.openConnection();
			conn.setReadTimeout(2*1000);
			conn.setConnectTimeout(2*1000);
			conn.connect();
			if ( conn.getResponseCode() == -1 ) {
				//msgLog("Index Page Bad Response: "+u.toExternalForm());
				Add_To_List("Error loading back main page after long form item! Potential overflow with form "+u.toExternalForm());
			}
		} catch (MalformedURLException e) {
			// should NEVER get here...
			e.printStackTrace();
		} catch ( SocketTimeoutException se ) {
			//msgLog("Index Page Response Timed Out: "+u.toExternalForm());
			Add_To_List("Could not load back main page after long form item! Potential overflow with form "+u.toExternalForm());
			// se.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if ( conn != null ) {
				conn.disconnect();
				conn = null;
			}
		}
	}

}
