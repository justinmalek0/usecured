package uSecured;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class GZip_Input extends Bin_Input {
    
    public static final String THREAD_NAME = "GZip Input";

	public GZip_Input(File f, Controller c, Log l) {
		super(f, c, l);
	}

	@Override
	protected InputStream get_Input_Stream() throws IOException {
		return new GZIPInputStream( new FileInputStream(get_File()));
	}

	@Override
	public String get_Thread_Name() {
        return GZip_Input.THREAD_NAME;
	}

	@Override
	public GZip_Input newCopy(File f) {
		return new GZip_Input(f, getController(), getLog());
	}

}
