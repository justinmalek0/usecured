package uSecured;

import java.util.TimerTask;

public class InterruptTask extends TimerTask {
	
	private final MicroSecured_Thread threadToInterrupt;
	
	public InterruptTask( MicroSecured_Thread t ) {
		threadToInterrupt = t;
	}

	/**
	 * @return the threadToInterrupt
	 */
	public MicroSecured_Thread getThreadToInterrupt() {
		return threadToInterrupt;
	}

	@Override
	public void run() {
		// threadToInterrupt.msgLog("Preparing to Interrupt");
		threadToInterrupt.getThread().interrupt();
	}

}
