package uSecured;

import java.util.List;

public class Parsed_File_Message extends Message {
	
	private List<Source_File> listFiles;

	public Parsed_File_Message(String m, List<Source_File> l) {
		super(Message_Type.Parsed_File, m);
		Files(l);
	}

	/**
	 * @return the listFiles
	 */
	public List<Source_File> Files() {
		return listFiles;
	}

	/**
	 * @param listFiles the listFiles to set
	 */
	public void Files(List<Source_File> listFiles) {
		this.listFiles = listFiles;
	}

}
