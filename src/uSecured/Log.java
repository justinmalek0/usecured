package uSecured;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ArrayBlockingQueue;

public class Log extends MicroSecured_Thread {
	
	public static final String THREAD_NAME = "uSecured Log";

	private File log_file;
	private boolean append;
	private boolean writeAppend;
    private PrintWriter printer = null;
    
	StringBuilder str = new StringBuilder();
    
    public Log( int q_size )
    {
    	setMsgQ( new ArrayBlockingQueue<Message>( q_size ) );
    	log_file = null;
    	setNew_file(false);
    	
    	setLog(this);
        
        start();
    }

    // TODO: MUST clean up the output file...
	@Override
	public void run() {
        while ( isRunning() )
        {
        	try {
				Message msg = recvMessage();

				System.out.println("Log Message: " + msg.getMsg());
				
				if(log_file == null)
				{
					str.append(msg.getMsg()+"\r\n");
					writeAppend = true;
				}
				else
				{
					try {
						printer = new PrintWriter( new BufferedWriter( new FileWriter( log_file, append)));
						// create the file the first time w/o append - all subsequent opens MUST have append!
						if ( append == false ) {
							append = true;
						}
						if(writeAppend) {
							printer.print(str.toString());
							writeAppend = false;
						}
						// write msg.getMsg() to log_file
						str.append(msg.getMsg()+"\r\n");
						printer.println(msg.getMsg());
						printer.flush();
						printer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
	}
	
	public String getLogBuffer() {
		return str.toString();
	}

    public void message( Message m )
    {
        try {
			getMsgQ().put( m );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

	public File getLog_file() {
		return log_file;
	}

	public void setLog_file(File log_file) {
		this.log_file = log_file;
	}

	public boolean isNew_file() {
		return append;
	}

	public void setNew_file(boolean append) {
		this.append = append;
	}

	@Override
	public String get_Thread_Name() {
		return Log.THREAD_NAME;
	}

	@Override
	public String toString() {
		return Log.THREAD_NAME;
	}

	@Override
	public Log newCopy() {
		return new Log(getMsgQ().size());
	}

}
