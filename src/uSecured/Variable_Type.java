package uSecured;

public enum Variable_Type {
	POINTER,
	BITS_8,
	BITS_16,
	BITS_32,
	BITS_64,
	STRUCT,
	NOT_SET,
	VOID,
}
