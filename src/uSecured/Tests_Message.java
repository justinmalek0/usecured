package uSecured;

public class Tests_Message extends Message {
	private Analysis_Test test;

	public Tests_Message(Message_Type t, String m, Analysis_Test at) {
		super(t, m);
		setTest(at);
	}

	public Analysis_Test getTest() {
		return test;
	}

	public void setTest(Analysis_Test test) {
		this.test = test;
	}

}
