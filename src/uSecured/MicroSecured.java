package uSecured;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JList;

public class MicroSecured {

	private Controller ctlr;
	private Log logging;
    private File log_file;
    private final JFileChooser fc = new JFileChooser();

	private JFrame frmUsecured;
	private JTextField txtLogFilePath;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmStartTests;
	private JMenuItem mntmExit;
	private JMenu mnConfiguration;
	private JMenuItem mntmObjectCodeTests;
	private JMenuItem mntmRuntimeTests;
	private JMenuItem mntmSourceCodeTests;
	private JButton btnRuntimeTests;
	private JButton btnObjectCodeTests;
	private JButton btnCodeTests;
	private JLabel lblConfiguration;
	private JProgressBar progressBar;
	private JButton btnRunTests;
	private JLabel lblTestList;
	private JLabel lblLogFile;
	private JList<Analysis_Test> listObjTests;
	private DefaultListModel<Analysis_Test> listmodel;
    private JScrollPane scrollTests;
    private JButton btnViewLog;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MicroSecured window = new MicroSecured();
					window.frmUsecured.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MicroSecured() {
		Log_Init( );
		Test_Control_Init( logging );

		initialize();
	}

	private void Test_Control_Init( Log l ) {
		ctlr = new Controller( 100, l, this );
	}

	private void Log_Init() {
		logging = new Log( 500 );
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmUsecured = new JFrame();
		frmUsecured.setResizable(false);
		frmUsecured.setTitle("uSecured");
		frmUsecured.setBounds(100, 100, 505, 297);
		frmUsecured.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
		menuBar = new JMenuBar();
		menuBar.setEnabled(false);
		menuBar.setVisible(false);
		frmUsecured.setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmStartTests = new JMenuItem("Start Tests...");
		mnFile.add(mntmStartTests);
		
		mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		
		mnConfiguration = new JMenu("Configuration");
		menuBar.add(mnConfiguration);
		
		mntmObjectCodeTests = new JMenuItem("Object Code Tests");
		mnConfiguration.add(mntmObjectCodeTests);
		
		mntmRuntimeTests = new JMenuItem("Runtime Tests");
		mnConfiguration.add(mntmRuntimeTests);
		
		mntmSourceCodeTests = new JMenuItem("Source Code Tests");
		mnConfiguration.add(mntmSourceCodeTests);
		frmUsecured.getContentPane().setLayout(null);
		
		btnRuntimeTests = new JButton("Runtime Tests");
		btnRuntimeTests.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		Config_Runtime_Test frame = new Config_Runtime_Test( ctlr, logging );
				frame.setVisible(true);
			}
		});
		btnRuntimeTests.setBounds(10, 72, 150, 30);
		frmUsecured.getContentPane().add(btnRuntimeTests);
		
		btnObjectCodeTests = new JButton("Object Code Tests");
		btnObjectCodeTests.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
				Config_Obj_Test frame = new Config_Obj_Test( ctlr, logging );
				frame.setVisible(true);
			}
		});
		btnObjectCodeTests.setBounds(10, 31, 150, 30);
		frmUsecured.getContentPane().add(btnObjectCodeTests);
		
		btnCodeTests = new JButton("Source Code Tests");
		btnCodeTests.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
				Config_SourceCode_Test frame = new Config_SourceCode_Test( ctlr, logging );
				frame.setVisible(true);
			}
		});
		btnCodeTests.setBounds(10, 113, 150, 30);
		frmUsecured.getContentPane().add(btnCodeTests);
		
		lblConfiguration = new JLabel("Configuration");
		lblConfiguration.setBounds(10, 11, 86, 14);
		frmUsecured.getContentPane().add(lblConfiguration);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(170, 223, 319, 14);
		frmUsecured.getContentPane().add(progressBar);
		
		btnRunTests = new JButton("Run Tests");
		btnRunTests.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
				ctlr.message( new Start_Message("Start Testing!") );
				btnRunTests.setEnabled(false);
			}
		});
		btnRunTests.setEnabled(false);
		btnRunTests.setBounds(170, 182, 155, 30);
		frmUsecured.getContentPane().add(btnRunTests);
		
		lblTestList = new JLabel("Test List");
		lblTestList.setBounds(170, 11, 55, 14);
		frmUsecured.getContentPane().add(lblTestList);
		
		txtLogFilePath = new JTextField();
		txtLogFilePath.setBounds(10, 168, 130, 20);
		frmUsecured.getContentPane().add(txtLogFilePath);
		txtLogFilePath.setColumns(10);
		
		lblLogFile = new JLabel("Log File:");
		lblLogFile.setBounds(10, 154, 46, 14);
		frmUsecured.getContentPane().add(lblLogFile);
        
        JButton btnBrowse = new JButton("Browse...");
        btnBrowse.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if ( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION )
        		{
        			log_file = fc.getSelectedFile();
        			txtLogFilePath.setText( log_file.getAbsolutePath() );
        		}
        	}
        });
        btnBrowse.setBounds(20, 189, 120, 23);
        frmUsecured.getContentPane().add(btnBrowse);
        
        listmodel = new DefaultListModel<Analysis_Test>();
        listObjTests = new JList<Analysis_Test>(listmodel);
        listObjTests.setBounds(170, 31, 319, 140);
        frmUsecured.getContentPane().add(listObjTests);
        
        scrollTests = new JScrollPane(listObjTests, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollTests.setBounds(170, 31, 319, 140);
        frmUsecured.getContentPane().add(scrollTests);
        
        JButton btnSetLogFile = new JButton("Set Log File");
        btnSetLogFile.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if ( ( log_file == null                                              ) ||
       				 ( !txtLogFilePath.getText().equals(log_file.getAbsolutePath() ) )    )
       			{
        			if( txtLogFilePath.getText().length() > 0 ) {
        				log_file = new File( txtLogFilePath.getText() );        				
        			} else {
        				JOptionPane.showMessageDialog(frmUsecured, "No Log File Selected", "Log File Error", JOptionPane.ERROR_MESSAGE);
        				return;
        			}
       			}
        		
        		logging.setNew_file(log_file.exists());
        		
        		if ( log_file.exists() && !log_file.canWrite() )
        		{
	        		logging.message( new Message( log_file.getAbsolutePath() + " must be write-able!" ) );
        		}
        			
    			logging.setLog_file(log_file);
        	}
        });
        btnSetLogFile.setBounds(20, 214, 120, 23);
        frmUsecured.getContentPane().add(btnSetLogFile);
        
        btnViewLog = new JButton("View Log");
        btnViewLog.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Log_Dialog ld = new Log_Dialog(frmUsecured.getOwner(), ctlr, logging);

        		ld.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        		ld.setVisible(true);
        	}
        });
        btnViewLog.setBounds(334, 182, 155, 30);
        frmUsecured.getContentPane().add(btnViewLog);
        
        debug_setup();
	}
	
	private void debug_setup() {
		txtLogFilePath.setText("C:\\Users\\justinmalek0\\Documents\\TESTLOG.txt");
	}

	public void update_test_list( Analysis_Test at ) {
		listmodel.addElement(at);
		btnRunTests.setEnabled(true);
		frmUsecured.validate();
		frmUsecured.repaint();
	}
	
	public void update_progress( int percent ) {
		progressBar.setValue(percent);
		if(percent == 100) {
			JOptionPane.showMessageDialog(frmUsecured, "Tests Complete!", "Progress", JOptionPane.INFORMATION_MESSAGE);
			progressBar.setValue(0);
			// listmodel.clear();
			listObjTests.clearSelection();
			frmUsecured.validate();
			frmUsecured.repaint();
			btnRunTests.setEnabled(false);
		}
	}
}
