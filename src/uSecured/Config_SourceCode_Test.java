package uSecured;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JList;

import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;

public class Config_SourceCode_Test extends JFrame {

	private static final long serialVersionUID = -2860489716240340330L;
	private JPanel contentPane;
	
	private Controller ctlr;
	private Log logging;
    private File scan_file;
    private final JFileChooser fc = new JFileChooser();
    private List<Source_Scan> List_Source_Tests;
	private JTextField txtFilePath;
	private JList<Source_Scan> listSourceTest;
	private JButton btnBrowse;
	private JButton btnSave;
	private JButton btnExit;
	private JLabel lblIncludePaths;
	private JScrollPane scrollPanePaths;
	private JTextArea txtrPaths;

	/**
	 * Create the frame.
	 */
	public Config_SourceCode_Test( Controller c, Log l) {
		
		ctlr = c;
		logging = l;
		
		scan_file = null;
		List_Source_Tests = new ArrayList<Source_Scan>();

        setResizable(false);
        setTitle("Source Code Tests");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 350, 394);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panelButton = new JPanel();
		contentPane.add(panelButton, BorderLayout.SOUTH);
		panelButton.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
    			boolean error = false;
    			List_Source_Tests.clear();
    			
    			if( scan_file == null){
    				if( !txtFilePath.getText().isEmpty() ) {
	    				scan_file = new File( txtFilePath.getText() );
    				} 
    			}
    			
    			if ( scan_file != null ) {
	    			if ( !txtFilePath.getText().equals(scan_file.getAbsolutePath() ) ) {
	    				scan_file = new File( txtFilePath.getText() );
	    			}
	    			if ( !scan_file.exists() ) {
	    				logging.message( new Message( "Please select an existing file.\r\n" + scan_file.getAbsolutePath() + " does not exist!" ) );
	    				error = true;
	    			}
	    			if ( !error && !scan_file.canRead() ) {
	    				logging.message( new Message( scan_file.getAbsolutePath() + " must be readable!" ) );
	    				error = true;
	    			}
    			} else {
    				if( txtrPaths.getText().isEmpty() ) {
    					logging.msgLog("A file or paths must be specified!");
    					error = true;
    				}
    			}

    			if ( !error ) {
    				Source_Input si = new Source_Input(ctlr, logging, scan_file, txtrPaths.getText());
    				
    				ctlr.msgController(new Source_File_Message("Source Input Message", si));
    				
    				List_Source_Tests = listSourceTest.getSelectedValuesList();
        			
        			if ( !List_Source_Tests.isEmpty() ) {
	        			for( Source_Scan test : List_Source_Tests ) {
		        			// logging.message( new Message( "Want to run: " + test ) );
		        			ctlr.message(new Source_Tests_Message("Source Scan Tests", test.newCopy(si)));
		        		}
        			}
        			else {
        				logging.message( new Message( "No Source Scan Tests Selected" ) );
        			}
    			}
			}
		});
		panelButton.add(btnSave);
		
		btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panelButton.add(btnExit);
		
		JPanel panelContent = new JPanel();
		contentPane.add(panelContent, BorderLayout.NORTH);
		GridBagLayout gbl_panelContent = new GridBagLayout();
		gbl_panelContent.columnWidths = new int[] {180, 0, 0};
		gbl_panelContent.rowHeights = new int[] {0, 100, 0, 0, 0, 100};
		gbl_panelContent.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_panelContent.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
		panelContent.setLayout(gbl_panelContent);
		
		JLabel lblTestList = new JLabel("Test List");
		GridBagConstraints gbc_lblTestList = new GridBagConstraints();
		gbc_lblTestList.gridwidth = 2;
		gbc_lblTestList.fill = GridBagConstraints.BOTH;
		gbc_lblTestList.insets = new Insets(0, 0, 5, 0);
		gbc_lblTestList.gridx = 0;
		gbc_lblTestList.gridy = 0;
		panelContent.add(lblTestList, gbc_lblTestList);
		
		JScrollPane scrollPaneTests = new JScrollPane();
		GridBagConstraints gbc_scrollPaneTests = new GridBagConstraints();
		gbc_scrollPaneTests.gridwidth = 2;
		gbc_scrollPaneTests.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPaneTests.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneTests.gridx = 0;
		gbc_scrollPaneTests.gridy = 1;
		panelContent.add(scrollPaneTests, gbc_scrollPaneTests);

        DefaultListModel<Source_Scan> listTests = new DefaultListModel<Source_Scan>();
        listTests.addElement(new Scan_Printf( ctlr, logging, null) );
        listTests.addElement(new Scan_Strcpy( ctlr, logging, null) );
        listTests.addElement(new Scan_Strcat( ctlr, logging, null) );
		listSourceTest = new JList<Source_Scan>(listTests);
		listSourceTest.setToolTipText("Select multiple by using Control+Click");
		scrollPaneTests.setViewportView(listSourceTest);
		
		JLabel lblFileFolderScan = new JLabel("File to Scan:");
		GridBagConstraints gbc_lblFileFolderScan = new GridBagConstraints();
		gbc_lblFileFolderScan.gridwidth = 2;
		gbc_lblFileFolderScan.insets = new Insets(0, 0, 5, 0);
		gbc_lblFileFolderScan.anchor = GridBagConstraints.WEST;
		gbc_lblFileFolderScan.gridx = 0;
		gbc_lblFileFolderScan.gridy = 2;
		panelContent.add(lblFileFolderScan, gbc_lblFileFolderScan);
		
		txtFilePath = new JTextField();
		GridBagConstraints gbc_txtFilePath = new GridBagConstraints();
		gbc_txtFilePath.insets = new Insets(0, 0, 5, 5);
		gbc_txtFilePath.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFilePath.gridx = 0;
		gbc_txtFilePath.gridy = 3;
		panelContent.add(txtFilePath, gbc_txtFilePath);
		txtFilePath.setColumns(10);
		
		btnBrowse = new JButton("Browse...");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
        		if ( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION )
        		{
        			scan_file = fc.getSelectedFile();
        			txtFilePath.setText( scan_file.getAbsolutePath() );
        		}
			}
		});
		GridBagConstraints gbc_btnBrowse = new GridBagConstraints();
		gbc_btnBrowse.insets = new Insets(0, 0, 5, 0);
		gbc_btnBrowse.gridx = 1;
		gbc_btnBrowse.gridy = 3;
		panelContent.add(btnBrowse, gbc_btnBrowse);
		
		lblIncludePaths = new JLabel("Include Paths:");
		GridBagConstraints gbc_lblIncludePaths = new GridBagConstraints();
		gbc_lblIncludePaths.insets = new Insets(0, 0, 5, 0);
		gbc_lblIncludePaths.gridwidth = 2;
		gbc_lblIncludePaths.anchor = GridBagConstraints.WEST;
		gbc_lblIncludePaths.gridx = 0;
		gbc_lblIncludePaths.gridy = 4;
		panelContent.add(lblIncludePaths, gbc_lblIncludePaths);
		
		scrollPanePaths = new JScrollPane();
		GridBagConstraints gbc_scrollPanePaths = new GridBagConstraints();
		gbc_scrollPanePaths.gridwidth = 2;
		gbc_scrollPanePaths.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPanePaths.fill = GridBagConstraints.BOTH;
		gbc_scrollPanePaths.gridx = 0;
		gbc_scrollPanePaths.gridy = 5;
		panelContent.add(scrollPanePaths, gbc_scrollPanePaths);
		
		txtrPaths = new JTextArea();
		txtrPaths.setToolTipText("Enter each path on a separate line.");
		scrollPanePaths.setViewportView(txtrPaths);
		
		debugSetup();
	}

	private void debugSetup() {
		txtFilePath.setText("C:\\Users\\justinmalek0\\Documents\\ssi_EISzCDR.c");
		
		listSourceTest.setSelectedIndex(0);
		
		txtrPaths.setText("C:\\Users\\justinmalek0\\Documents\\usecuredtest");
	}

}
