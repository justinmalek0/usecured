package uSecured;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Arrays;

public class URL_Overflow extends Runtime_Web {

	public static final int OVERFLOW_MAX_COUNT = 8;
	public static final int OVERFLOW_INCREMENT = 1024;

	public URL_Overflow(Controller c, Log l, String _url_) {
		super(c, l, _url_);
		for( int i = 1 ; i <= URL_Overflow.OVERFLOW_MAX_COUNT ; i++ ) {
			char[] ov = new char[i * URL_Overflow.OVERFLOW_INCREMENT];
			Arrays.fill(ov, 'a');
			Add_To_Possibles(new String(ov)+".htm");
		}
	}

	@Override
	public URL_Overflow newCopy(String url) {
		return new URL_Overflow(getController(), getLog(), url);
	}

	@Override
	public String toString() {
		return super.toString() + " URL Overflow";
	}

	@Override
	protected void extraHTMLParsing(BufferedReader reader, URL u) {
		// reload the main page to see if the unit is still responding
		URL baseURL = null;
		HttpURLConnection conn = null;
		try {
			baseURL = new URL( u.getProtocol() + "://" + u.getHost() );
			conn = setupConnection(baseURL);
			conn.setReadTimeout(2*1000);
			conn.setConnectTimeout(2*1000);
			conn.connect();
			if ( conn.getResponseCode() == -1 ) {
				Add_To_List("Error loading back main page after long URL! Potential URL overflow w/ URL length = " + u.getFile().length());
			}
		} catch (MalformedURLException e) {
			// should NEVER get here...
			e.printStackTrace();
		} catch ( SocketTimeoutException se ) {
			//msgLog("Index Page Response Timed Out: "+u.toExternalForm());
			Add_To_List("Could not load back main page after long URL! Potential URL overflow w/ URL length = " + u.getFile().length());
			// se.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if ( conn != null ) {
				conn.disconnect();
				conn = null;
			}
		}
	}

	@Override
	protected boolean checkResponse(URL u, HttpURLConnection conn)
			throws IOException {
		int response = conn.getResponseCode();
		
		if( response == -1 ) {
			Add_To_List(u.getHost() + " got bad response! Potential URL overflow with URL length = " + u.getFile().length());
			return false;
		}
		
		return true;
	}

	@Override
	protected void handleError(IOException e, String u, String p) {
		if ( e.getMessage() == null ){
			//msgLog("IO Error with the request: "+u+" with URL length = "+p.length());
		} else {
			//msgLog("IO Error ("+e.getMessage()+") with the request: "+u+" with URL length = "+p.length());
		}
	}

	@Override
	protected void handleError(FileNotFoundException fe, String u, String p) {
		//msgLog("Page not Available: "+u+" with URL length = "+p.length());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof URL_Overflow)) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}

		return true;
	}

}
