package uSecured;

import java.util.List;

public abstract class Source_Scan extends Analysis_Test {
	
	private Source_Input input;

	public Source_Scan(Controller c, Log l, Source_Input si) {
		super(c, l);
		setInput(si);
	}

	@Override
	public void run() {
		// TODO: receive from this.InputQueue - each file parsed?
		// TODO: add hits via Add_To_List(String)
		// NOTES: Probably have to scan everything first before checking...
    	//msgLog(new Message("Source Scan Started"));
        while ( isRunning() )
        {
	        try {
				Message msg = getMsgQ().take();
				
				if(msg instanceof Stop_Message) {
					finish();
				} else if ( msg instanceof Parsed_File_Message )
				{
					Parsed_File_Message pfmsg = (Parsed_File_Message)msg;
					scan(pfmsg.Files());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
	}
	
	public abstract String SearchString();
	protected abstract void check(Source_File file, String function, String[] args);
	
	protected boolean scan(List<Source_File> files) {
		for(Source_File file : files ){
			String tempLines = file.Lines().trim();
			int printIdx;
			try {
				while((printIdx = tempLines.indexOf(SearchString())) != -1){
					// Find the beginning of the command
					String commandWhole = findWholeCommand(tempLines, printIdx);
					
					tempLines = tempLines.substring(printIdx + SearchString().length());
					
					String cleanCommand = cleanUpCommand(commandWhole);
					
					//msgLog("scmd>["+cleanCommand+"]<");
					
					String function = parseFunction(cleanCommand);
					String[] args = parseFunctionArgs(cleanCommand);
					
					//msgLog("cmd> "+function);
					//for(String arg : args){
					//	msgLog("arg> "+arg);
					//}
					
					check(file, function, args);
				}
			} catch (IndexOutOfBoundsException e ) {
				// intentionally do nothing...
				// We just want to keep parsing the next file
				// e.printStackTrace();
				// NOTE: This might happen because we are not using source code that has already
				// been through a preprocessor. So the #'s could screw something up with the parsing
			}
		}
		return true;
	}

	@Override
	public String toString() {
		if ( input == null )
		{
			return "Source Scan";
		}
		
		if(isComplete()) {
			return "Completed: " + input.getFile().getName() + ": Source Scan";
		}
		
		return input.getFile().getName() + ": Source Scan";
	}
	
	public abstract Source_Scan newCopy(Source_Input si);

	@Override
	public Source_Scan newCopy() {
		return newCopy(getInput());
	}

	@Override
	public void finish() {
		//msgLog("Source_Scan Ended");
		super.finish();
	}

	public Source_Input getInput() {
		return input;
	}

	public void setInput(Source_Input input) {
		this.input = input;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((input == null) ? 0 : input.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Source_Scan other = (Source_Scan) obj;
		if (input == null) {
			if (other.input != null)
				return false;
		} else if (!input.equals(other.input))
			return false;
		return true;
	}
	
	protected int findCommandEnd(String searchString, int index) throws IndexOutOfBoundsException {
		boolean isInQuotes = false;
		try {
			while( isInQuotes || ( !isInQuotes && searchString.charAt(index) != ';' ) ) {
				if( searchString.charAt(index) == '"' && ( index == 0 || searchString.charAt(index-1) != '\\' ) ) {
					isInQuotes = !isInQuotes;
				}
				index++;
			}
		} catch ( IndexOutOfBoundsException e ) {
			return -1;
		}
		return index;
	}
	
	protected int findCommandStart(String searchString, int index) throws IndexOutOfBoundsException {
		while(index > 0 && Character.isLetter(searchString.charAt(index-1))) {
			index--;
		}
		return index;
	}

	protected int findArgsEnd(String searchString, int index) throws IndexOutOfBoundsException {
		boolean isInQuotes = false;
		boolean keepLooping = true;
		int countOpenParens = 0;
		int countCloseParens = 0;
		int retIndex = -1;
		try {
			while( keepLooping || ( isInQuotes || ( !isInQuotes && ( searchString.charAt(index) == '(' || searchString.charAt(index) == ')' ) ) ) ) {
				if( searchString.charAt(index) == '"' && ( index == 0 || searchString.charAt(index-1) != '\\' ) ) {
					isInQuotes = !isInQuotes;
				} else if ( !isInQuotes ) {
					if( searchString.charAt(index) == '(' ) {
						countOpenParens++;
					} else if( searchString.charAt(index) == ')' ) {
						countCloseParens++;
						if ( countOpenParens == countCloseParens ) {
							keepLooping = false;
							retIndex = index;
						}
					}
				}
				index++;
			}
		} catch ( IndexOutOfBoundsException e ) {
		}
		return retIndex;
	}
	
	protected String cleanUpCommand(String cmd) {
		return cleanMultiLineStrings( cleanSpaces( compressWhitespaceToSpace( cmd ) ) );
	}

	// remove all the whitespace and make it a single space
	protected String compressWhitespaceToSpace(String str) {
		return str.replaceAll("\\s+", " ");
	}
	
	// 	remove the spaces before and after the commas/parenthesis
	protected String cleanSpaces(String str) {
		return str.replaceAll("\\s*(\\(|\\)|,)\\s*", "$1"); 
	}

	// remove '" "' - this happens when you place string literals on different lines
	protected String cleanMultiLineStrings(String str) {
		return str.replaceAll("(?<!\\\\)\" \"", "");
	}

	// remove '" "' - this happens when you place string literals on different lines
	protected String findWholeCommand(String str, int idxStart) throws IndexOutOfBoundsException {
		return str.substring(findCommandStart( str, idxStart ), findCommandEnd(str, idxStart));
	}
	
	protected String parseFunction(String function) throws IndexOutOfBoundsException {
		return function.substring(0, function.indexOf("("));
	}
	
	protected String[] parseFunctionArgs(String function) throws IndexOutOfBoundsException {
		// Find the indexes of function arguments
		int idxArgStart = function.indexOf("(");
		int idxArgEnd = findArgsEnd(function, idxArgStart );
		// split on ',' except those found within quotes
		return function.substring(idxArgStart+1,idxArgEnd).split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
	}

}
