package uSecured;

import java.util.concurrent.BlockingQueue;

public abstract class MicroSecured_Thread implements Runnable {

	private Controller controller = null;
	private Log log = null;
	private Thread thread = null;
	private volatile boolean isRunning = false;
	private volatile boolean isComplete = false;
    private BlockingQueue<Message> MsgQ = null;
	
	// public MicroSecured_Thread() {
	// 	setRunning(false);
	// 	setThread(null);
	// 	setMsgQ(null);
	// 	setController(null);
	// 	setLog(null);
	// }

	@Override
	public abstract void run( );
	public abstract String get_Thread_Name( );
	@Override
	public abstract String toString();
	public abstract MicroSecured_Thread newCopy();
	
	public void start() {
		create();
		start_run();
	}
	
	public void create() {
		setThread(new Thread( this, get_Thread_Name() ));
	}
	
	public void start_run() throws IllegalThreadStateException, NullPointerException {
		if ( thread == null ) {
			throw new NullPointerException("Thread must be created first!");
		}
		setRunning(true);
		thread.start();
	}
	
	public void finish() {
		setComplete(true);
		setRunning(false);
		msgController(new Thread_Complete_Message(this+" complete!", this));
	}

	public boolean isRunning() {
		return isRunning;
	}

	private void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public boolean isComplete() {
		return isComplete;
	}
	
	private void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}
	
	public BlockingQueue<Message> getMsgQ() {
		return MsgQ;
	}

	public void setMsgQ(BlockingQueue<Message> msgQ) {
		MsgQ = msgQ;
	}

	public Thread getThread() {
		return thread;
	}

	private void setThread(Thread thread) {
		this.thread = thread;
	}

	public Controller getController() {
		return controller;
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}
	
	public void msgLog( Message m ) throws NullPointerException {
		if ( log == null ) {
			throw new NullPointerException("Log must be created first!");
		}
		log.message(m);
	}
	
	public void msgLog( String s ) throws NullPointerException {
		if ( log == null ) {
			throw new NullPointerException("Log must be created first!");
		}
		log.message(new Message(s));
	}
	
	public void msgController( Message m ) throws NullPointerException {
		if ( controller == null ) {
			throw new NullPointerException("Controller must be created first!");
		}
		controller.message(m);
	}
	
	public Message recvMessage( ) throws InterruptedException,NullPointerException {
		if ( MsgQ == null ) {
			throw new NullPointerException("Message Q must be created first!");
		}
		return MsgQ.take();
	}
	
	protected void waitForOtherMSThread( MicroSecured_Thread t ) {
		while( !t.isComplete() || t.isRunning() ) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
